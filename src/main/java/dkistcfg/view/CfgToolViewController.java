/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dkistcfg.view;

import dkistcfg.Dkistcfg;
import dkistcfg.model.Camera;
import dkistcfg.model.BsConfig;
import javafx.collections.FXCollections;

import javafx.collections.ObservableList;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;



/**
 *
 * @author fwoeger
 */
public class CfgToolViewController {
  
  private Dkistcfg appCfg;
  
  // needs to be public so Analyze object can have access (RootLayoutController)
  public final BsConfig bsC = new BsConfig();

  private static final int VBICAM1 = 0;
  private static final int VBI393 = 0;
  private static final int VBI430 = 1;
  private static final int VBI450 = 2;
  private static final int VBI486 = 3;

  private static final int VBICAM2 = 1;
  private static final int VBI656 = 0;
  private static final int VBI668 = 1;
  private static final int VBI705 = 2;
  private static final int VBI789 = 3;

  private static final int VISPCAM1 = 2;
  private static final int VISPCAM2 = 3;
  private static final int VISPCAM3 = 4;
  private static final int VISP = 0;
  private static final ObservableList<String> WLVISP = FXCollections.observableArrayList();

  private static final int VTFCAM1 = 5;
  private static final int VTF589 = 0;
  private static final int VTF630 = 1;
  private static final int VTF656 = 2;
  private static final int VTF854 = 3;

  private static final int DLNCAM1 = 6;
  private static final int DLNCAM2 = 7;
  private static final int DLNCAM3 = 8;
  private static final int DLN = 0;
  private static final ObservableList<String> WLDLN1 = FXCollections.observableArrayList();
  private static final ObservableList<String> WLDLN2 = FXCollections.observableArrayList();
  private static final ObservableList<String> WLDLN3 = FXCollections.observableArrayList();

  // VBI
  // blue
  @FXML
  private CheckBox cbVBIc1393;
  @FXML
  private TextField tfVBIc1393W;
  @FXML
  private CheckBox cbVBIc1430;
  @FXML
  private TextField tfVBIc1430W;
  @FXML
  private CheckBox cbVBIc1450;
  @FXML
  private TextField tfVBIc1450W;
  @FXML
  private CheckBox cbVBIc1486;
  @FXML
  private TextField tfVBIc1486W;
  @FXML
  private TextField tfVBIc1MDR;
  // red
  @FXML
  private CheckBox cbVBIc2656;
  @FXML
  private TextField tfVBIc2656W;
  @FXML
  private CheckBox cbVBIc2668;
  @FXML
  private TextField tfVBIc2668W;
  @FXML
  private CheckBox cbVBIc2705;
  @FXML
  private TextField tfVBIc2705W;
  @FXML
  private CheckBox cbVBIc2789;
  @FXML
  private TextField tfVBIc2789W;
  @FXML
  private TextField tfVBIc2MDR;
  
  // ViSP
  // Camera 1
  @FXML
  private ComboBox cbVISPc1WL;
  @FXML
  private TextField tfVISPc1W;
  @FXML
  private TextField tfVISPc1MDR;
  // Camera 2
  @FXML
  private ComboBox cbVISPc2WL;
  @FXML
  private TextField tfVISPc2W;
  @FXML
  private TextField tfVISPc2MDR;
  // Camera 3
  @FXML
  private ComboBox cbVISPc3WL;
  @FXML
  private TextField tfVISPc3W;
  @FXML
  private TextField tfVISPc3MDR;

  // VTF
  // polarimetry flag (affects data rate)
  @FXML
  private CheckBox cbVTFP;

  @FXML
  private CheckBox cbVTFc1589;
  @FXML
  private TextField tfVTFc1589W;
  @FXML
  private CheckBox cbVTFc1630;
  @FXML
  private TextField tfVTFc1630W;
  @FXML
  private CheckBox cbVTFc1656;
  @FXML
  private TextField tfVTFc1656W;
  @FXML
  private CheckBox cbVTFc1854;
  @FXML
  private TextField tfVTFc1854W;
  @FXML
  private TextField tfVTFc1MDR;
  
  // DLNIRSP
  // Camera 1
  @FXML
  private ComboBox cbDLNc1WL;
  @FXML
  private TextField tfDLNc1W;
  @FXML
  private TextField tfDLNc1MDR;
  // Camera 2
  @FXML
  private ComboBox cbDLNc2WL;
  @FXML
  private TextField tfDLNc2W;
  @FXML
  private TextField tfDLNc2MDR;
  // Camera 3
  @FXML
  private ComboBox cbDLNc3WL;
  @FXML
  private TextField tfDLNc3W;
  @FXML
  private TextField tfDLNc3MDR;
  
  // Labels
  @FXML
  private Label vbiBLabelOk;
  @FXML
  private Label vbiBLabelNo;
  @FXML
  private Label vbiRLabelOk;
  @FXML
  private Label vbiRLabelNo;
  @FXML
  private Label vispLabelOk;
  @FXML
  private Label vispLabelNo;
  @FXML
  private Label vtfLabelOk;
  @FXML
  private Label vtfLabelNo;
  @FXML
  private Label dlLabelOk;
  @FXML
  private Label dlLabelNo;
  
  @FXML
  private Label cl2Label;
  @FXML
  private Label cl2aLabel;
  @FXML
  private Label cl3Label;
  @FXML
  private Label cl3aLabel;
  
  @FXML
  private Label dataRateLabel;
  
  // ComboBox for equivalent solutions
  @FXML
  private ComboBox solutions;
  
  // Line Chart
  @FXML
  private LineChart<Number,Number> lineChart;
  @FXML
  private NumberAxis xAxis;
  @FXML
  private NumberAxis yAxis;
  
  // Wavelength distribution 'bar diagram'
  @FXML
  private StackPane pWavelength;
  @FXML
  private StackPane pThroughput;
  @FXML
  private ImageView pVBIb;
  @FXML
  private ImageView pVBIr;
  @FXML
  private ImageView pVISP;
  @FXML
  private ImageView pVTF;
  @FXML
  private ImageView pDL;

  @FXML
  private void initialize() {
    // VBI
    cbVBIc1393.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VBICAM1).setCamEnabled(VBI393, newValue);
        tfVBIc1393W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });
    cbVBIc1430.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VBICAM1).setCamEnabled(VBI430, newValue);
        tfVBIc1430W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });
    cbVBIc1450.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VBICAM1).setCamEnabled(VBI450, newValue);
        tfVBIc1450W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });
    cbVBIc1486.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VBICAM1).setCamEnabled(VBI486, newValue);
        tfVBIc1486W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });
    tfVBIc1393W.setDisable(true);
    tfVBIc1393W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVBIc1393W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI393, input);
          tfVBIc1393W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI393)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI393, input);
            tfVBIc1393W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI393, input);
            tfVBIc1393W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI393)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVBIc1430W.setDisable(true);
    tfVBIc1430W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVBIc1430W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI430, input);
          tfVBIc1430W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI430)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI430, input);
            tfVBIc1430W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI430, input);
            tfVBIc1430W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI430)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVBIc1450W.setDisable(true);
    tfVBIc1450W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVBIc1450W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI450, input);
          tfVBIc1450W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI450)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI450, input);
            tfVBIc1450W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI450, input);
            tfVBIc1450W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI450)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVBIc1486W.setDisable(true);
    tfVBIc1486W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVBIc1486W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI486, input);
          tfVBIc1486W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI486)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI486, input);
            tfVBIc1486W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM1).setCamWeight(VBI486, input);
            tfVBIc1486W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI486)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVBIc1MDR.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfVBIc1MDR.getText();
        Float input;
        try {
          input = Float.parseFloat(inString);
          if (input < 0) input = (float)Camera.DEFAULTDATARATE;
          appCfg.getCameraData().get(VBICAM1).setCamMaxDataRate(input);
          //tfVBIc1MDR.setText(String.valueOf(appCfg.getCameraData().get(VBICAM1).getCamMaxDataRate()));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = (float)Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VBICAM1).setCamMaxDataRate(input);
            tfVBIc1MDR.clear();
          } else {
            input = Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VBICAM1).setCamMaxDataRate(input);
            tfVBIc1MDR.setText(String.valueOf(appCfg.getCameraData().get(VBICAM1).getCamMaxDataRate()));
          }
        }
        this.computeConfAndUpdate();
    });

    cbVBIc2656.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VBICAM2).setCamEnabled(VBI656, newValue);
        tfVBIc2656W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });
    cbVBIc2668.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VBICAM2).setCamEnabled(VBI668, newValue);
        tfVBIc2668W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });
    cbVBIc2705.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VBICAM2).setCamEnabled(VBI705, newValue);
        tfVBIc2705W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });
    cbVBIc2789.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VBICAM2).setCamEnabled(VBI789, newValue);
        tfVBIc2789W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });
    tfVBIc2656W.setDisable(true);
    tfVBIc2656W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVBIc2656W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI656, input);
          tfVBIc2656W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI656)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI656, input);
            tfVBIc2656W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI656, input);
            tfVBIc2656W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI656)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVBIc2668W.setDisable(true);
    tfVBIc2668W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVBIc2668W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI668, input);
          tfVBIc2668W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI668)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI668, input);
            tfVBIc2668W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI668, input);
            tfVBIc2668W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI668)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVBIc2705W.setDisable(true);
    tfVBIc2705W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVBIc2705W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI705, input);
          tfVBIc2705W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI705)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI705, input);
            tfVBIc2705W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI705, input);
            tfVBIc2705W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI705)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVBIc2789W.setDisable(true);
    tfVBIc2789W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVBIc2789W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI789, input);
          tfVBIc2789W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI789)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI789, input);
            tfVBIc2789W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VBICAM2).setCamWeight(VBI789, input);
            tfVBIc2789W.setText(String.valueOf(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI789)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVBIc2MDR.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfVBIc2MDR.getText();
        Float input;
        try {
          input = Float.parseFloat(inString);
          if (input < 0) input = (float)Camera.DEFAULTDATARATE;
          appCfg.getCameraData().get(VBICAM2).setCamMaxDataRate(input);
          //tfVBIc2MDR.setText(String.valueOf(appCfg.getCameraData().get(VBICAM2).getCamMaxDataRate()));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = (float)Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VBICAM2).setCamMaxDataRate(input);
            tfVBIc2MDR.clear();
          } else {
            input = Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VBICAM2).setCamMaxDataRate(input);
            tfVBIc2MDR.setText(String.valueOf(appCfg.getCameraData().get(VBICAM2).getCamMaxDataRate()));
          }
        }
        this.computeConfAndUpdate();
    });

    // ViSP
    // general wavelength list
    WLVISP.add("Disabled.");
    WLVISP.add("393.3 nm - Ca II K");
    WLVISP.add("396.8 nm - Ca II H");
    WLVISP.add("410.0 nm - H-delta");
    WLVISP.add("422.7 nm - Ca I");
    WLVISP.add("430.5 nm - G-band");
    WLVISP.add("434.0 nm - H-gamma");
    WLVISP.add("460.7 nm - Sr I");
    WLVISP.add("486.2 nm - H-beta");
    WLVISP.add("517.3 nm - Mg I");
    WLVISP.add("525.0 nm - Fe I ");
    WLVISP.add("530.3 nm - Fe XIV");
    WLVISP.add("557.6 nm - Fe I");
    WLVISP.add("587.6 nm - He D3");
    WLVISP.add("589.0 nm - Na D");
    WLVISP.add("617.3 nm - Fe I");
    WLVISP.add("630.2 nm - Fe I");
    WLVISP.add("637.4 nm - Fe X");
    WLVISP.add("656.3 nm - H-alpha");
    WLVISP.add("705.4 nm - TiO");
    WLVISP.add("705.9 nm - Fe XV");
    WLVISP.add("709.0 nm - Fe I");
    WLVISP.add("769.9 nm - K I");
    WLVISP.add("777.0 nm - O I");
    WLVISP.add("789.2 nm - Fe XI");
    WLVISP.add("854.2 nm - Ca II");

    // Camera 1
    cbVISPc1WL.getItems().addAll(WLVISP);
    cbVISPc1WL.setEditable(true);
    cbVISPc1WL.setValue(cbVISPc1WL.getItems().get(0));
    cbVISPc1WL.getSelectionModel().selectedItemProperty().addListener(
      (observable, oldValue, newValue) -> {
        if (cbVISPc1WL.getEditor().getText().contains("Disabled")) {
          appCfg.getCameraData().get(VISPCAM1).setCamEnabled(VISP, false);
          appCfg.getCameraData().get(VISPCAM1).setCamWL(VISP, 0);
          cbVISPc1WL.setValue(cbVISPc1WL.getItems().get(0));
          tfVISPc1W.setDisable(true);
        } else {
          String[] inString = cbVISPc1WL.getEditor().getText().split("[.]*[\\.][.]*", 2);
          Integer input;
          try {
            input = Integer.parseInt(inString[0]);
            if (input < 0) input = 0;
            appCfg.getCameraData().get(VISPCAM1).setCamEnabled(VISP, true);
            appCfg.getCameraData().get(VISPCAM1).setCamWL(VISP, input);
            tfVISPc1W.setDisable(false);
          } catch (NumberFormatException e) {
            if (inString[0].isEmpty()) {
              input = 0;
              appCfg.getCameraData().get(VISPCAM1).setCamEnabled(VISP, false);
              appCfg.getCameraData().get(VISPCAM1).setCamWL(VISP, input);
              cbVISPc1WL.setValue(cbVISPc1WL.getItems().get(0));
              tfVISPc1W.setDisable(true);
            } else {
              input = 0;
              appCfg.getCameraData().get(VISPCAM1).setCamEnabled(VISP, false);
              appCfg.getCameraData().get(VISPCAM1).setCamWL(VISP, input);
              cbVISPc1WL.setValue(cbVISPc1WL.getItems().get(0));
              tfVISPc1W.setDisable(true);
            }
          }
        }
        this.computeConfAndUpdate();
    });
    tfVISPc1W.setDisable(true);
    tfVISPc1W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfVISPc1W.getText();
        Integer input;
        try {
          input = Integer.parseInt(inString);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VISPCAM1).setCamWeight(VISP, input);
          tfVISPc1W.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM1).getCamWeight(VISP)));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VISPCAM1).setCamWeight(VISP, input);
            tfVISPc1W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VISPCAM1).setCamWeight(VISP, input);
            tfVISPc1W.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM1).getCamWeight(VISP)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVISPc1MDR.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfVISPc1MDR.getText();
        Float input;
        try {
          input = Float.parseFloat(inString);
          if (input < 0) input = (float)Camera.DEFAULTDATARATE;
          appCfg.getCameraData().get(VISPCAM1).setCamMaxDataRate(input);
          //tfVISPc1MDR.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM1).getCamMaxDataRate()));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = (float)Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VISPCAM1).setCamMaxDataRate(input);
            tfVISPc1MDR.clear();
          } else {
            input = Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VISPCAM1).setCamMaxDataRate(input);
            tfVISPc1MDR.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM1).getCamMaxDataRate()));
          }
        }
        this.computeConfAndUpdate();
    });

    cbVISPc2WL.getItems().addAll(WLVISP);
    cbVISPc2WL.setEditable(true);
    cbVISPc2WL.setValue(cbVISPc2WL.getItems().get(0));
    cbVISPc2WL.getSelectionModel().selectedItemProperty().addListener(
      (observable, oldValue, newValue) -> {
        if (cbVISPc2WL.getEditor().getText().contains("Disabled")) {
          appCfg.getCameraData().get(VISPCAM2).setCamEnabled(VISP, false);
          appCfg.getCameraData().get(VISPCAM2).setCamWL(VISP, 0);
          cbVISPc2WL.setValue(cbVISPc2WL.getItems().get(0));
          tfVISPc2W.setDisable(true);
        } else {
          String[] inString = cbVISPc2WL.getEditor().getText().split("[.]*[\\.][.]*", 2);
          Integer input;
          try {
            input = Integer.parseInt(inString[0]);
            if (input < 0) input = 0;
            appCfg.getCameraData().get(VISPCAM2).setCamEnabled(VISP, true);
            appCfg.getCameraData().get(VISPCAM2).setCamWL(VISP, input);
            tfVISPc2W.setDisable(false);
          } catch (NumberFormatException e) {
            if (inString[0].isEmpty()) {
              input = 0;
              appCfg.getCameraData().get(VISPCAM2).setCamEnabled(VISP, false);
              appCfg.getCameraData().get(VISPCAM2).setCamWL(VISP, input);
              cbVISPc2WL.setValue(cbVISPc1WL.getItems().get(0));
              tfVISPc2W.setDisable(true);
            } else {
              input = 0;
              appCfg.getCameraData().get(VISPCAM2).setCamEnabled(VISP, false);
              appCfg.getCameraData().get(VISPCAM2).setCamWL(VISP, input);
              cbVISPc2WL.setValue(cbVISPc2WL.getItems().get(0));
              tfVISPc2W.setDisable(true);
            }
          }
        }
        this.computeConfAndUpdate();
    });
    tfVISPc2W.setDisable(true);
    tfVISPc2W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfVISPc2W.getText();
        Integer input;
        try {
          input = Integer.parseInt(inString);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VISPCAM2).setCamWeight(VISP, input);
          tfVISPc2W.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM2).getCamWeight(VISP)));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VISPCAM2).setCamWeight(VISP, input);
            tfVISPc2W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VISPCAM2).setCamWeight(VISP, input);
            tfVISPc2W.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM2).getCamWeight(VISP)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVISPc2MDR.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfVISPc2MDR.getText();
        Float input;
        try {
          input = Float.parseFloat(inString);
          if (input < 0) input = (float)Camera.DEFAULTDATARATE;
          appCfg.getCameraData().get(VISPCAM2).setCamMaxDataRate(input);
          //tfVISPc2MDR.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM2).getCamMaxDataRate()));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = (float)Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VISPCAM2).setCamMaxDataRate(input);
            tfVISPc2MDR.clear();
          } else {
            input = Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VISPCAM2).setCamMaxDataRate(input);
            tfVISPc2MDR.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM2).getCamMaxDataRate()));
          }
        }
        this.computeConfAndUpdate();
    });

    cbVISPc3WL.getItems().addAll(WLVISP);
    cbVISPc3WL.setEditable(true);
    cbVISPc3WL.setValue(cbVISPc3WL.getItems().get(0));
    cbVISPc3WL.getSelectionModel().selectedItemProperty().addListener(
      (observable, oldValue, newValue) -> {
        if (cbVISPc3WL.getEditor().getText().contains("Disabled")) {
          appCfg.getCameraData().get(VISPCAM3).setCamEnabled(VISP, false);
          appCfg.getCameraData().get(VISPCAM3).setCamWL(VISP, 0);
          cbVISPc3WL.setValue(cbVISPc3WL.getItems().get(0));
          tfVISPc3W.setDisable(true);
        } else {
          String[] inString = cbVISPc3WL.getEditor().getText().split("[.]*[\\.][.]*", 2);
          Integer input;
          try {
            input = Integer.parseInt(inString[0]);
            if (input < 0) input = 0;
            appCfg.getCameraData().get(VISPCAM3).setCamEnabled(VISP, true);
            appCfg.getCameraData().get(VISPCAM3).setCamWL(VISP, input);
            tfVISPc3W.setDisable(false);
          } catch (NumberFormatException e) {
            if (inString[0].isEmpty()) {
              input = 0;
              appCfg.getCameraData().get(VISPCAM3).setCamEnabled(VISP, false);
              appCfg.getCameraData().get(VISPCAM3).setCamWL(VISP, input);
              cbVISPc3WL.setValue(cbVISPc3WL.getItems().get(0));
              tfVISPc3W.setDisable(true);
            } else {
              input = 0;
              appCfg.getCameraData().get(VISPCAM3).setCamEnabled(VISP, false);
              appCfg.getCameraData().get(VISPCAM3).setCamWL(VISP, input);
              cbVISPc3WL.setValue(cbVISPc3WL.getItems().get(0));
              tfVISPc3W.setDisable(true);
            }
          }
        }
        this.computeConfAndUpdate();
    });
    tfVISPc3W.setDisable(true);
    tfVISPc3W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfVISPc3W.getText();
        Integer input;
        try {
          input = Integer.parseInt(inString);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VISPCAM3).setCamWeight(VISP, input);
          tfVISPc3W.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM3).getCamWeight(VISP)));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VISPCAM3).setCamWeight(VISP, input);
            tfVISPc3W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VISPCAM3).setCamWeight(VISP, input);
            tfVISPc3W.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM3).getCamWeight(VISP)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVISPc3MDR.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfVISPc3MDR.getText();
        Float input;
        try {
          input = Float.parseFloat(inString);
          if (input < 0) input = (float)Camera.DEFAULTDATARATE;
          appCfg.getCameraData().get(VISPCAM3).setCamMaxDataRate(input);
          //tfVISPc3MDR.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM3).getCamMaxDataRate()));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = (float)Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VISPCAM3).setCamMaxDataRate(input);
            tfVISPc3MDR.clear();
          } else {
            input = Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VISPCAM3).setCamMaxDataRate(input);
            tfVISPc3MDR.setText(String.valueOf(appCfg.getCameraData().get(VISPCAM3).getCamMaxDataRate()));
          }
        }
        this.computeConfAndUpdate();
    });

    // VTF
    cbVTFP.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VTFCAM1).setCamModFlag(newValue);
        this.computeConfAndUpdate();
    });
    cbVTFc1589.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VTFCAM1).setCamEnabled(VTF589, newValue);
        tfVTFc1589W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });
    cbVTFc1630.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VTFCAM1).setCamEnabled(VTF630, newValue);
        tfVTFc1630W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });
    cbVTFc1656.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VTFCAM1).setCamEnabled(VTF656, newValue);
        tfVTFc1656W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });
    cbVTFc1854.selectedProperty().addListener(
      (observable, oldValue, newValue) -> {
        appCfg.getCameraData().get(VTFCAM1).setCamEnabled(VTF854, newValue);
        tfVTFc1854W.setDisable(!newValue);
        this.computeConfAndUpdate();
    });

    tfVTFc1589W.setDisable(true);
    tfVTFc1589W.focusedProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVTFc1589W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF589, input);
          tfVTFc1589W.setText(String.valueOf(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF589)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF589, input);
            tfVTFc1589W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF589, input);
            tfVTFc1589W.setText(String.valueOf(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF589)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVTFc1630W.setDisable(true);
    tfVTFc1630W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVTFc1630W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF630, input);
          tfVTFc1630W.setText(String.valueOf(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF630)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF630, input);
            tfVTFc1630W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF630, input);
            tfVTFc1630W.setText(String.valueOf(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF630)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVTFc1656W.setDisable(true);
    tfVTFc1656W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVTFc1656W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF656, input);
          tfVTFc1656W.setText(String.valueOf(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF656)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF656, input);
            tfVTFc1656W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF656, input);
            tfVTFc1656W.setText(String.valueOf(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF656)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVTFc1854W.setDisable(true);
    tfVTFc1854W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfVTFc1854W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF854, input);
          tfVTFc1854W.setText(String.valueOf(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF854)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF854, input);
            tfVTFc1854W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(VTFCAM1).setCamWeight(VTF854, input);
            tfVTFc1854W.setText(String.valueOf(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF854)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfVTFc1MDR.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfVTFc1MDR.getText();
        Float input;
        try {
          input = Float.parseFloat(inString);
          if (input < 0) input = (float)Camera.DEFAULTDATARATE;
          appCfg.getCameraData().get(VTFCAM1).setCamMaxDataRate(input);
          //tfVTFc1MDR.setText(String.valueOf(appCfg.getCameraData().get(VTFCAM1).getCamMaxDataRate()));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = (float)Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VTFCAM1).setCamMaxDataRate(input);
            tfVTFc1MDR.clear();
          } else {
            input = Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(VTFCAM1).setCamMaxDataRate(input);
            tfVTFc1MDR.setText(String.valueOf(appCfg.getCameraData().get(VTFCAM1).getCamMaxDataRate()));
          }
        }
        this.computeConfAndUpdate();
    });

    // DL-NIRSP
    WLDLN1.add("Disabled.");
    WLDLN1.add("530.3 nm - Fe XIV");
    WLDLN1.add("587.6 nm - He I D3");
    WLDLN1.add("630.2 nm - Fe I");
    WLDLN1.add("789.2 nm - Fe XI");
    WLDLN1.add("854.2 nm - Ca II");

    WLDLN2.add("Disabled.");
    WLDLN2.add("1074.7 nm - Fe XIII");
    WLDLN2.add("1079.8 nm - Fe XIII");
    WLDLN2.add("1083.0 nm - He I/Si I");

    WLDLN3.add("Disabled.");
    WLDLN3.add("1430.0 nm - Si X");
    WLDLN3.add("1565.0 nm - Fe I");

    // DLN Camera 1
    cbDLNc1WL.getItems().addAll(WLDLN1);
    cbDLNc1WL.setEditable(false);
    cbDLNc1WL.setValue(cbDLNc1WL.getItems().get(0));
    cbDLNc1WL.getSelectionModel().selectedItemProperty().addListener(
      (observable, oldValue, newValue) -> {
        if (cbDLNc1WL.getSelectionModel().getSelectedItem().toString().contains("Disabled")) {
          appCfg.getCameraData().get(DLNCAM1).setCamEnabled(DLN, false);
          appCfg.getCameraData().get(DLNCAM1).setCamWL(DLN, 0);
          cbDLNc1WL.setValue(cbDLNc1WL.getItems().get(0));
          tfDLNc1W.setDisable(true);
        } else {
          String[] inString = cbDLNc1WL.getSelectionModel().getSelectedItem().toString().split("[.]*[\\.][.]*", 2);
          Integer input;
          try {
            input = Integer.parseInt(inString[0]);
            if (input < 0) input = 0;
            appCfg.getCameraData().get(DLNCAM1).setCamEnabled(DLN, true);
            appCfg.getCameraData().get(DLNCAM1).setCamWL(DLN, input);
            {
              int i;
              for (i=0; i<WLDLN1.size(); i++) {
                if (WLDLN1.get(i).contains(input+".")) break;
              }
              if (i<cbDLNc1WL.getItems().size()) cbDLNc1WL.setValue(cbDLNc1WL.getItems().get(i));
            }
            tfDLNc1W.setDisable(false);
          } catch (NumberFormatException e) {
            if (inString[0].isEmpty()) {
              input = 0;
              appCfg.getCameraData().get(DLNCAM1).setCamEnabled(DLN, false);
              appCfg.getCameraData().get(DLNCAM1).setCamWL(DLN, input);
              cbDLNc1WL.setValue(cbDLNc1WL.getItems().get(0));
              tfDLNc1W.setDisable(true);
            } else {
              input = 0;
              appCfg.getCameraData().get(DLNCAM1).setCamEnabled(DLN, false);
              appCfg.getCameraData().get(DLNCAM1).setCamWL(DLN, input);
              cbDLNc1WL.setValue(cbDLNc1WL.getItems().get(0));
              tfDLNc1W.setDisable(true);
            }
          }
        }
        this.computeConfAndUpdate();
    });
    tfDLNc1W.setDisable(true);
    tfDLNc1W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfDLNc1W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(DLNCAM1).setCamWeight(DLN, input);
          tfDLNc1W.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM1).getCamWeight(DLN)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(DLNCAM1).setCamWeight(DLN, input);
            tfDLNc1W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(DLNCAM1).setCamWeight(DLN, input);
            tfDLNc1W.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM1).getCamWeight(DLN)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfDLNc1MDR.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfDLNc1MDR.getText();
        Float input;
        try {
          input = Float.parseFloat(inString);
          if (input < 0) input = (float)Camera.DEFAULTDATARATE;
          appCfg.getCameraData().get(DLNCAM1).setCamMaxDataRate(input);
          //tfDLNc1MDR.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM1).getCamMaxDataRate()));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = (float)Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(DLNCAM1).setCamMaxDataRate(input);
            tfDLNc1MDR.clear();
          } else {
            input = Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(DLNCAM1).setCamMaxDataRate(input);
            tfDLNc1MDR.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM1).getCamMaxDataRate()));
          }
        }
        this.computeConfAndUpdate();
    });

    // Camera 2
    cbDLNc2WL.getItems().addAll(WLDLN2);
    cbDLNc2WL.setEditable(false);
    cbDLNc2WL.setValue(cbDLNc2WL.getItems().get(0));
    cbDLNc2WL.getSelectionModel().selectedItemProperty().addListener(
      (observable, oldValue, newValue) -> {
        if (cbDLNc2WL.getSelectionModel().getSelectedItem().toString().contains("Disabled")) {
          appCfg.getCameraData().get(DLNCAM2).setCamEnabled(DLN, false);
          appCfg.getCameraData().get(DLNCAM2).setCamWL(DLN, 0);
          cbDLNc2WL.setValue(cbDLNc2WL.getItems().get(0));
          tfDLNc2W.setDisable(true);
        } else {
          String[] inString = cbDLNc2WL.getSelectionModel().getSelectedItem().toString().split("[.]*[\\.][.]*", 2);
          Integer input;
          try {
            input = Integer.parseInt(inString[0]);
            if (input < 0) input = 0;
            appCfg.getCameraData().get(DLNCAM2).setCamEnabled(DLN, true);
            appCfg.getCameraData().get(DLNCAM2).setCamWL(DLN, input);
            {
              int i;
              for (i=0; i<WLDLN2.size(); i++) {
                if (WLDLN2.get(i).contains(input+".")) break;
              }
              if (i<cbDLNc2WL.getItems().size()) cbDLNc2WL.setValue(cbDLNc2WL.getItems().get(i));
            }
            tfDLNc2W.setDisable(false);
          } catch (NumberFormatException e) {
            if (inString[0].isEmpty()) {
              input = 0;
              appCfg.getCameraData().get(DLNCAM2).setCamEnabled(DLN, false);
              appCfg.getCameraData().get(DLNCAM2).setCamWL(DLN, input);
              cbDLNc2WL.setValue(cbDLNc2WL.getItems().get(0));
              tfDLNc2W.setDisable(true);
            } else {
              input = 0;
              appCfg.getCameraData().get(DLNCAM2).setCamEnabled(DLN, false);
              appCfg.getCameraData().get(DLNCAM2).setCamWL(DLN, input);
              cbDLNc2WL.setValue(cbDLNc2WL.getItems().get(0));
              tfDLNc2W.setDisable(true);
            }
          }
        }
        this.computeConfAndUpdate();
    });
    tfDLNc2W.setDisable(true);
    tfDLNc2W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfDLNc2W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(DLNCAM2).setCamWeight(DLN, input);
          tfDLNc2W.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM2).getCamWeight(DLN)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(DLNCAM2).setCamWeight(DLN, input);
            tfDLNc2W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(DLNCAM2).setCamWeight(DLN, input);
            tfDLNc2W.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM2).getCamWeight(DLN)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfDLNc2MDR.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfDLNc2MDR.getText();
        Float input;
        try {
          input = Float.parseFloat(inString);
          if (input < 0) input = (float)Camera.DEFAULTDATARATE;
          appCfg.getCameraData().get(DLNCAM2).setCamMaxDataRate(input);
          //tfDLNc2MDR.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM2).getCamMaxDataRate()));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = (float)Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(DLNCAM2).setCamMaxDataRate(input);
            tfDLNc2MDR.clear();
          } else {
            input = Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(DLNCAM2).setCamMaxDataRate(input);
            tfDLNc2MDR.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM2).getCamMaxDataRate()));
          }
        }
        this.computeConfAndUpdate();
    });

    // Camera 3
    cbDLNc3WL.getItems().addAll(WLDLN3);
    cbDLNc3WL.setEditable(false);
    cbDLNc3WL.setValue(cbDLNc3WL.getItems().get(0));
    cbDLNc3WL.getSelectionModel().selectedItemProperty().addListener(
      (observable, oldValue, newValue) -> {
        if (cbDLNc3WL.getSelectionModel().getSelectedItem().toString().contains("Disabled")) {
          appCfg.getCameraData().get(DLNCAM3).setCamEnabled(DLN, false);
          appCfg.getCameraData().get(DLNCAM3).setCamWL(DLN, 0);
          cbDLNc3WL.setValue(cbDLNc3WL.getItems().get(0));
          tfDLNc3W.setDisable(true);
        } else {
          String[] inString = cbDLNc3WL.getSelectionModel().getSelectedItem().toString().split("[.]*[\\.][.]*", 2);
          Integer input;
          try {
            input = Integer.parseInt(inString[0]);
            if (input < 0) input = 0;
            appCfg.getCameraData().get(DLNCAM3).setCamEnabled(DLN, true);
            appCfg.getCameraData().get(DLNCAM3).setCamWL(DLN, input);
            {
              int i;
              for (i=0; i<WLDLN3.size(); i++) {
                if (WLDLN3.get(i).contains(input+".")) break;
              }
              if (i<cbDLNc3WL.getItems().size()) cbDLNc3WL.setValue(cbDLNc3WL.getItems().get(i));
            }
            tfDLNc3W.setDisable(false);
          } catch (NumberFormatException e) {
            if (inString[0].isEmpty()) {
              input = 0;
              appCfg.getCameraData().get(DLNCAM3).setCamEnabled(DLN, false);
              appCfg.getCameraData().get(DLNCAM3).setCamWL(DLN, input);
              cbDLNc3WL.setValue(cbDLNc3WL.getItems().get(0));
              tfDLNc3W.setDisable(true);
            } else {
              input = 0;
              appCfg.getCameraData().get(DLNCAM3).setCamEnabled(DLN, false);
              appCfg.getCameraData().get(DLNCAM3).setCamWL(DLN, input);
              cbDLNc3WL.setValue(cbDLNc3WL.getItems().get(0));
              tfDLNc3W.setDisable(true);
            }
          }
        }
        this.computeConfAndUpdate();
    });
    tfDLNc3W.setDisable(true);
    tfDLNc3W.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String[] inString = tfDLNc3W.getText().split("[.]*[\\.][.]*", 2);
        Integer input;
        try {
          input = Integer.parseInt(inString[0]);
          if (input < Camera.DEFAULTWEIGHT) input = Camera.DEFAULTWEIGHT;
          appCfg.getCameraData().get(DLNCAM3).setCamWeight(DLN, input);
          tfDLNc3W.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM3).getCamWeight(DLN)));
        } catch (NumberFormatException e) {
          if (inString[0].isEmpty()) {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(DLNCAM3).setCamWeight(DLN, input);
            tfDLNc3W.clear();
          } else {
            input = Camera.DEFAULTWEIGHT;
            appCfg.getCameraData().get(DLNCAM3).setCamWeight(DLN, input);
            tfDLNc3W.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM3).getCamWeight(DLN)));
          }
        }
        this.computeConfAndUpdate();
    });
    tfDLNc3MDR.textProperty().addListener(
      (observable, oldValue, newValue) -> {
        String inString = tfDLNc3MDR.getText();
        Float input;
        try {
          input = Float.parseFloat(inString);
          if (input < 0) input = (float)Camera.DEFAULTDATARATE;
          appCfg.getCameraData().get(DLNCAM3).setCamMaxDataRate(input);
          //tfDLNc3MDR.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM3).getCamMaxDataRate()));
        } catch (NumberFormatException e) {
          if (inString.isEmpty()) {
            input = (float)Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(DLNCAM3).setCamMaxDataRate(input);
            tfDLNc3MDR.clear();
          } else {
            input = Camera.DEFAULTDATARATE;
            appCfg.getCameraData().get(DLNCAM3).setCamMaxDataRate(input);
            tfDLNc3MDR.setText(String.valueOf(appCfg.getCameraData().get(DLNCAM3).getCamMaxDataRate()));
          }
        }
        this.computeConfAndUpdate();
    });

    // Tooltips for the solution labels
    Tooltip.install(vispLabelOk, new Tooltip("Successful diagnostic in the ViSP"));
    Tooltip.install(vispLabelNo, new Tooltip("Diagnostic limited in the ViSP"));
    Tooltip.install(vbiBLabelOk, new Tooltip("Successful diagnostic in the VBI blue"));
    Tooltip.install(vbiBLabelNo, new Tooltip("Diagnostic limited in the VBI blue"));
    Tooltip.install(vbiRLabelOk, new Tooltip("Successful diagnostic in the VBI red"));
    Tooltip.install(vbiRLabelNo, new Tooltip("Diagnostic limited in the VBI red"));
    Tooltip.install(vtfLabelOk, new Tooltip("Successful diagnostic in the VTF"));
    Tooltip.install(vtfLabelNo, new Tooltip("Diagnostic limited in the VTF"));
    Tooltip.install(dlLabelOk, new Tooltip("Successful diagnostic in the DL-NIRSP"));
    Tooltip.install(dlLabelNo, new Tooltip("Diagnostic limited in the DL-NIRSP"));
    Tooltip.install(cl2Label, new Tooltip("Suggested optic for CL2 for this option"));
    Tooltip.install(cl2aLabel, new Tooltip("Suggested optic for CL2a for this option"));
    Tooltip.install(cl3Label, new Tooltip("Suggested optic for CL3 for this option"));
    Tooltip.install(cl3aLabel, new Tooltip("Suggested optic for CL3a for this option"));
    Tooltip.install(dataRateLabel, new Tooltip("Max. Data Rate (if input was given)"));
    
    solutions.getSelectionModel().selectedItemProperty().addListener(
      (observable, oldValue, newValue) -> {
        updateInfo();
    });
    Tooltip.install(solutions, new Tooltip("List of options that achieve the same score."));

    lineChart.setLegendVisible(true);
    lineChart.setCreateSymbols(false);
    xAxis.setLabel("wavelength [nm]");
    xAxis.setForceZeroInRange(false);
    xAxis.setAutoRanging(false);
    xAxis.setLowerBound(380);
    xAxis.setUpperBound(1900);
    xAxis.setTickLabelGap(20);
    yAxis.setLabel("reflectance [%]");
    yAxis.setAutoRanging(false);
    yAxis.setLowerBound(0);
    yAxis.setUpperBound(100);
    yAxis.setTickLabelGap(10);
    lineChart.getData().clear();
    lineChart.getData().add(setLineChartData(this.bsC, "cbs465"));
    lineChart.getData().add(setLineChartData(this.bsC, "cbs555"));
    lineChart.getData().add(setLineChartData(this.bsC, "cbs643"));
    lineChart.getData().add(setLineChartData(this.bsC, "cbs680"));
    lineChart.getData().add(setLineChartData(this.bsC, "cbs950"));
    lineChart.getData().add(setLineChartData(this.bsC, "cwfcbs1"));
    lineChart.getData().add(setLineChartData(this.bsC, "cw1"));
    lineChart.getData().add(setLineChartData(this.bsC, "cm1"));
    
    pWavelength.boundsInLocalProperty().addListener(
      (observable, oldValue, newValue) -> {
        Double width = pWavelength.getWidth();
        Double height= pWavelength.getHeight();
        
        this.setWavelengthBar(width);

        pVBIb.setFitWidth(width);
        pVBIb.setFitHeight(0.9*height);
        pVBIr.setFitWidth(width);
        pVBIr.setFitHeight(0.9*height);
        pVTF.setFitWidth(width);
        pVTF.setFitHeight(0.9*height);
        pVISP.setFitWidth(width);
        pVISP.setFitHeight(0.9*height);
        pDL.setFitWidth(width);
        pDL.setFitHeight(0.9*height);
    });
    
    pThroughput.boundsInLocalProperty().addListener(
      (observable, oldValue, newValue) -> {
        Double width = pThroughput.getWidth();

        this.setThroughputBar(width);
    });
    
  }
  
  /**
   * Is called by the main application to give a reference back to itself.
   * 
   * @param appCfg
   */
  public void setAppCfg(Dkistcfg appCfg) {
    this.appCfg = appCfg;
  }

  /**
   * Create a line plot
   */
  private XYChart.Series setLineChartData(BsConfig bsconfig, String which) {
    XYChart.Series series = new XYChart.Series<>();

    for (int i=0; i<bsconfig.getWLOptic("wl".concat(which)).size(); i+=10) {
      series.getData().add(new XYChart.Data<>(bsconfig.getWLOptic("wl".concat(which)).get(i), bsconfig.getOptic(which).get(i)));
    }
    
    series.setName(which);

    return series;
  }

  /**
   * Create wavelength bar plot
   */
  private void setWavelengthBar(Double width) {
    double[] wl = {380., 520., 656., 1083., 1900.};
    
    pWavelength.getChildren().clear();
    double currentWidth = 0.0;
    for (int i=1; i<wl.length; i++) {
        Stop[] stops;
        // setting up some gradients
        if (i<3) { // these will go from blue (H=270) to h-alpha be red (H=0)
          stops = new Stop[] { 
            new Stop(0, Color.hsb(270.-270.*(wl[i-1]-wl[0])/(wl[wl.length-3]-wl[0]), 1, 1)), 
            new Stop(1, Color.hsb(270.-270.*(wl[i]-wl[0])/(wl[wl.length-3]-wl[0]), 1, 1))
          };
        } else {   // these will do some gradient stuff from red (H=0,S=1,B=1) to white (H=0,S=0,B=1)
          stops = new Stop[] {
            new Stop(0, Color.hsb(0, 1-(wl[i-1]-wl[wl.length-3])/(wl[wl.length-1]-wl[wl.length-3]), 1)), 
            new Stop(1, Color.hsb(0, 1-(wl[i]-wl[wl.length-3])/(wl[wl.length-1]-wl[wl.length-3]), 1))
          };
        }
        
        LinearGradient linear = new LinearGradient(0,0,1,0,true,CycleMethod.NO_CYCLE, stops);
        
        Rectangle rB = new Rectangle();

        rB.setFill(linear);
        rB.setStroke(Color.BLACK);
        rB.setStrokeWidth(0.7);
        rB.setX(currentWidth);
        // note: subtracing 1.0 in the formula appears to circumvent an edge overrun
        rB.widthProperty().bind(pWavelength.widthProperty().multiply( ((wl[i]-wl[i-1]-1.0)/(wl[wl.length-1]-wl[0])) ));
        rB.heightProperty().bind(pWavelength.heightProperty().multiply(0.25));
        Tooltip tB = new Tooltip(wl[i-1]+" - "+wl[i]+" nm");
        tB.setAutoHide(false);
        Tooltip.install(rB, tB);
        currentWidth += rB.getWidth();
        pWavelength.getChildren().add(rB);
        StackPane.setAlignment(rB, Pos.TOP_CENTER);
    }
  }
  
  /**
   * Create wavelength bar plot
   */
  private void setThroughputBar(Double width) {
    
    pThroughput.getChildren().clear();
    Stop[] stops;
    stops = new Stop[] { 
      new Stop(0, Color.hsb(0, 0, 0)), 
      new Stop(1, Color.hsb(0, 0, 1))
    };
    
    LinearGradient linear = new LinearGradient(0,0,1,0,true,CycleMethod.NO_CYCLE, stops);
    
    Rectangle rB = new Rectangle();

    rB.setFill(linear);
    rB.setStroke(Color.BLACK);
    rB.setStrokeWidth(0.7);
    rB.widthProperty().bind(pThroughput.widthProperty().multiply(0.995));
    rB.heightProperty().bind(pThroughput.heightProperty().multiply(0.25));
    Tooltip tB = new Tooltip("0 - 100%");
    tB.setAutoHide(false);
    Tooltip.install(rB, tB);
    pThroughput.getChildren().add(rB);
    StackPane.setAlignment(rB, Pos.TOP_CENTER);
  }
  
  public void onLoad() {
    ObservableList<Camera> cameraData = appCfg.getCameraData();
    
    // VBI blue
    if (appCfg.getCameraData().get(VBICAM1).getCamEnabled(VBI393)) {
      cbVBIc1393.setSelected(true);
      tfVBIc1393W.setDisable(false);
      if (appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI393) > 1)
        tfVBIc1393W.setText(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI393).toString());
      else
        tfVBIc1393W.clear();
    } else {
      cbVBIc1393.setSelected(false);
      tfVBIc1393W.setDisable(true);
      if (appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI393) > 1)
        tfVBIc1393W.setText(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI393).toString());
      else
        tfVBIc1393W.clear();
    }
    if (appCfg.getCameraData().get(VBICAM1).getCamEnabled(VBI430)) {
      cbVBIc1430.setSelected(true);
      tfVBIc1430W.setDisable(false);
      if (appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI430) > 1)
        tfVBIc1430W.setText(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI430).toString());
      else
        tfVBIc1430W.clear();
    } else {
      cbVBIc1430.setSelected(false);
      tfVBIc1430W.setDisable(true);
      if (appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI430) > 1)
        tfVBIc1430W.setText(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI430).toString());
      else
        tfVBIc1430W.clear();
    }
    if (appCfg.getCameraData().get(VBICAM1).getCamEnabled(VBI450)) {
      cbVBIc1450.setSelected(true);
      tfVBIc1450W.setDisable(false);
      if (appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI450) > 1)
        tfVBIc1450W.setText(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI450).toString());
      else
        tfVBIc1450W.clear();
    } else {
      cbVBIc1450.setSelected(false);
      tfVBIc1450W.setDisable(true);
      if (appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI450) > 1)
        tfVBIc1450W.setText(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI450).toString());
      else
        tfVBIc1450W.clear();
    }
    if (appCfg.getCameraData().get(VBICAM1).getCamEnabled(VBI486)) {
      cbVBIc1486.setSelected(true);
      tfVBIc1486W.setDisable(false);
      if (appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI486) > 1)
        tfVBIc1486W.setText(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI486).toString());
      else
        tfVBIc1486W.clear();
    } else {
      cbVBIc1486.setSelected(false);
      tfVBIc1486W.setDisable(true);
      if (appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI486) > 1)
        tfVBIc1486W.setText(appCfg.getCameraData().get(VBICAM1).getCamWeight(VBI486).toString());
      else
        tfVBIc1486W.clear();
    }
    if (appCfg.getCameraData().get(VBICAM1).getCamMaxDataRate() > 0)
      tfVBIc1MDR.setText(appCfg.getCameraData().get(VBICAM1).getCamMaxDataRate().toString());
    else
      tfVBIc1MDR.clear();
    
    // VBI red
    if (appCfg.getCameraData().get(VBICAM2).getCamEnabled(VBI656)) {
      cbVBIc2656.setSelected(true);
      tfVBIc2656W.setDisable(false);
      if (appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI656) > 1)
        tfVBIc2656W.setText(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI656).toString());
      else
        tfVBIc2656W.clear();
   } else {
      cbVBIc2656.setSelected(false);
      tfVBIc2656W.setDisable(true);
      if (appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI656) > 1)
        tfVBIc2656W.setText(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI656).toString());
      else
        tfVBIc2656W.clear();
    }
    if (appCfg.getCameraData().get(VBICAM2).getCamEnabled(VBI668)) {
      cbVBIc2668.setSelected(true);
      tfVBIc2668W.setDisable(false);
      if (appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI668) > 1)
        tfVBIc2668W.setText(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI668).toString());
      else
        tfVBIc2668W.clear();
    } else {
      cbVBIc2668.setSelected(false);
      tfVBIc2668W.setDisable(true);
      if (appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI668) > 1)
        tfVBIc2668W.setText(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI668).toString());
      else
        tfVBIc2668W.clear();
    }
    if (appCfg.getCameraData().get(VBICAM2).getCamEnabled(VBI705)) {
      cbVBIc2705.setSelected(true);
      tfVBIc2705W.setDisable(false);
      if (appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI705) > 1)
        tfVBIc2705W.setText(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI705).toString());
      else
        tfVBIc2705W.clear();
    } else {
      cbVBIc2705.setSelected(false);
      tfVBIc2705W.setDisable(true);
      if (appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI705) > 1)
        tfVBIc2705W.setText(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI705).toString());
      else
        tfVBIc2705W.clear();
    }
    if (appCfg.getCameraData().get(VBICAM2).getCamEnabled(VBI789)) {
      cbVBIc2789.setSelected(true);
      tfVBIc2789W.setDisable(false);
      if (appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI789) > 1)
        tfVBIc2789W.setText(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI789).toString());
      else
        tfVBIc2789W.clear();
    } else {
      cbVBIc2789.setSelected(false);
      tfVBIc2789W.setDisable(true);
      if (appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI789) > 1)
        tfVBIc2789W.setText(appCfg.getCameraData().get(VBICAM2).getCamWeight(VBI789).toString());
      else
        tfVBIc2789W.clear();
    }
    if (appCfg.getCameraData().get(VBICAM2).getCamMaxDataRate() > 0)
      tfVBIc2MDR.setText(appCfg.getCameraData().get(VBICAM2).getCamMaxDataRate().toString());
    else
      tfVBIc2MDR.clear();

    // ViSP 1
    if (appCfg.getCameraData().get(VISPCAM1).getCamEnabled(VISP)) {
      tfVISPc1W.setDisable(false);
      cbVISPc1WL.getEditor().setText(appCfg.getCameraData().get(VISPCAM1).getCamWL(VISP).toString());
      if (appCfg.getCameraData().get(VISPCAM1).getCamWeight(VISP) > 1)
        tfVISPc1W.setText(appCfg.getCameraData().get(VISPCAM1).getCamWeight(VISP).toString());
      else
        tfVISPc1W.clear();
    } else {
      cbVISPc1WL.setValue(cbVISPc1WL.getItems().get(0));
      tfVISPc1W.setDisable(true);
      if (appCfg.getCameraData().get(VISPCAM1).getCamWeight(VISP) > 1)
        tfVISPc1W.setText(appCfg.getCameraData().get(VISPCAM1).getCamWeight(VISP).toString());
      else
        tfVISPc1W.clear();
    }
    if (appCfg.getCameraData().get(VISPCAM1).getCamMaxDataRate() > 0)
      tfVISPc1MDR.setText(appCfg.getCameraData().get(VISPCAM1).getCamMaxDataRate().toString());
    else
      tfVISPc1MDR.clear();
    
    // ViSP 2
    if (appCfg.getCameraData().get(VISPCAM2).getCamEnabled(VISP)) {
      tfVISPc2W.setDisable(false);
      cbVISPc2WL.getEditor().setText(appCfg.getCameraData().get(VISPCAM2).getCamWL(VISP).toString());
      if (appCfg.getCameraData().get(VISPCAM2).getCamWeight(VISP) > 1)
        tfVISPc2W.setText(appCfg.getCameraData().get(VISPCAM2).getCamWeight(VISP).toString());
      else
        tfVISPc2W.clear();
    } else {
      cbVISPc2WL.setValue(cbVISPc2WL.getItems().get(0));
      tfVISPc2W.setDisable(true);
      if (appCfg.getCameraData().get(VISPCAM2).getCamWeight(VISP) > 1)
        tfVISPc2W.setText(appCfg.getCameraData().get(VISPCAM2).getCamWeight(VISP).toString());
      else
        tfVISPc2W.clear();
    }
    if (appCfg.getCameraData().get(VISPCAM2).getCamMaxDataRate() > 0)
      tfVISPc2MDR.setText(appCfg.getCameraData().get(VISPCAM2).getCamMaxDataRate().toString());
    else
      tfVISPc2MDR.clear();

    // ViSP 3
    if (appCfg.getCameraData().get(VISPCAM3).getCamEnabled(VISP)) {
      tfVISPc3W.setDisable(false);
      cbVISPc3WL.getEditor().setText(appCfg.getCameraData().get(VISPCAM3).getCamWL(VISP).toString());
      if (appCfg.getCameraData().get(VISPCAM3).getCamWeight(VISP) > 1)
        tfVISPc3W.setText(appCfg.getCameraData().get(VISPCAM3).getCamWeight(VISP).toString());
      else
        tfVISPc3W.clear();
    } else {
      cbVISPc3WL.setValue(cbVISPc3WL.getItems().get(0));
      tfVISPc3W.setDisable(true);
      if (appCfg.getCameraData().get(VISPCAM3).getCamWeight(VISP) > 1)
        tfVISPc3W.setText(appCfg.getCameraData().get(VISPCAM3).getCamWeight(VISP).toString());
      else
        tfVISPc3W.clear();
    }
    if (appCfg.getCameraData().get(VISPCAM3).getCamMaxDataRate() > 0)
      tfVISPc3MDR.setText(appCfg.getCameraData().get(VISPCAM3).getCamMaxDataRate().toString());
    else
      tfVISPc3MDR.clear();

    // VTF
    cbVTFP.selectedProperty().setValue(appCfg.getCameraData().get(VTFCAM1).getCamModFlag());
    if (appCfg.getCameraData().get(VTFCAM1).getCamEnabled(VTF589)) {
      cbVTFc1589.setSelected(true);
      tfVTFc1589W.setDisable(false);
      if (appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF589) > 1)
        tfVTFc1589W.setText(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF589).toString());
      else
        tfVTFc1589W.clear();
    } else {
      cbVTFc1589.setSelected(false);
      tfVTFc1589W.setDisable(true);
      if (appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF589) > 1)
        tfVTFc1589W.setText(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF589).toString());
      else
        tfVTFc1589W.clear();
    }
    if (appCfg.getCameraData().get(VTFCAM1).getCamEnabled(VTF630)) {
      cbVTFc1630.setSelected(true);
      tfVTFc1630W.setDisable(false);
      if (appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF630) > 1)
        tfVTFc1630W.setText(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF630).toString());
      else
        tfVTFc1630W.clear();
    } else {
      cbVTFc1630.setSelected(false);
      tfVTFc1630W.setDisable(true);
      if (appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF630) > 1)
        tfVTFc1630W.setText(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF630).toString());
      else
        tfVTFc1630W.clear();
    }
    if (appCfg.getCameraData().get(VTFCAM1).getCamEnabled(VTF656)) {
      cbVTFc1656.setSelected(true);
      tfVTFc1656W.setDisable(false);
      if (appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF656) > 1)
        tfVTFc1656W.setText(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF656).toString());
      else
        tfVTFc1656W.clear();
    } else {
      cbVTFc1656.setSelected(false);
      tfVTFc1656W.setDisable(true);
      if (appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF656) > 1)
        tfVTFc1656W.setText(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF656).toString());
      else
        tfVTFc1656W.clear();
    }
    if (appCfg.getCameraData().get(VTFCAM1).getCamEnabled(VTF854)) {
      cbVTFc1854.setSelected(true);
      tfVTFc1854W.setDisable(false);
      if (appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF854) > 1)
        tfVTFc1854W.setText(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF854).toString());
      else
        tfVTFc1854W.clear();
    } else {
      cbVTFc1854.setSelected(false);
      tfVTFc1854W.setDisable(true);
      if (appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF854) > 1)
        tfVTFc1854W.setText(appCfg.getCameraData().get(VTFCAM1).getCamWeight(VTF854).toString());
      else
        tfVTFc1854W.clear();
    }
    if (appCfg.getCameraData().get(VTFCAM1).getCamMaxDataRate() > 0)
      tfVTFc1MDR.setText(appCfg.getCameraData().get(VTFCAM1).getCamMaxDataRate().toString());
    else
      tfVTFc1MDR.clear();
    
     // DLN 1
    if (appCfg.getCameraData().get(DLNCAM1).getCamEnabled(DLN)) {
      tfDLNc1W.setDisable(false);
      {
        int i;
        for (i=0; i<WLDLN1.size(); i++) {
          if (WLDLN1.get(i).contains(appCfg.getCameraData().get(DLNCAM1).getCamWL(DLN).toString()+".")) break;
        }
        if (i<cbDLNc1WL.getItems().size()) cbDLNc1WL.setValue(cbDLNc1WL.getItems().get(i));
      }
      if (appCfg.getCameraData().get(DLNCAM1).getCamWeight(DLN) > 1)
        tfDLNc1W.setText(appCfg.getCameraData().get(DLNCAM1).getCamWeight(DLN).toString());
      else
        tfDLNc1W.clear();
    } else {
      cbDLNc1WL.setValue(cbDLNc1WL.getItems().get(0));
      tfDLNc1W.setDisable(true);
      if (appCfg.getCameraData().get(DLNCAM1).getCamWeight(DLN) > 1)
        tfDLNc1W.setText(appCfg.getCameraData().get(DLNCAM1).getCamWeight(DLN).toString());
      else
        tfDLNc1W.clear();
    }
    if (appCfg.getCameraData().get(DLNCAM1).getCamMaxDataRate() > 0)
      tfDLNc1MDR.setText(appCfg.getCameraData().get(DLNCAM1).getCamMaxDataRate().toString());
    else
      tfDLNc1MDR.clear();
    
     // DLN 2
    if (appCfg.getCameraData().get(DLNCAM2).getCamEnabled(DLN)) {
      tfDLNc2W.setDisable(false);
      {
        int i;
        for (i=0; i<WLDLN2.size(); i++) {
          if (WLDLN2.get(i).contains(appCfg.getCameraData().get(DLNCAM2).getCamWL(DLN).toString()+".")) break;
        }
        if (i<cbDLNc2WL.getItems().size()) cbDLNc2WL.setValue(cbDLNc2WL.getItems().get(i));
      }
      if (appCfg.getCameraData().get(DLNCAM2).getCamWeight(DLN) > 1)
        tfDLNc2W.setText(appCfg.getCameraData().get(DLNCAM2).getCamWeight(DLN).toString());
      else
        tfDLNc2W.clear();
    } else {
      cbDLNc2WL.setValue(cbDLNc2WL.getItems().get(0));
      tfDLNc2W.setDisable(true);
      if (appCfg.getCameraData().get(DLNCAM2).getCamWeight(DLN) > 1)
        tfDLNc2W.setText(appCfg.getCameraData().get(DLNCAM2).getCamWeight(DLN).toString());
      else
        tfDLNc2W.clear();
    }
    if (appCfg.getCameraData().get(DLNCAM2).getCamMaxDataRate() > 0)
      tfDLNc2MDR.setText(appCfg.getCameraData().get(DLNCAM2).getCamMaxDataRate().toString());
    else
      tfDLNc2MDR.clear();
    
     // DLN 3
    if (appCfg.getCameraData().get(DLNCAM3).getCamEnabled(DLN)) {
      tfDLNc3W.setDisable(false);
      {
        int i;
        for (i=0; i<WLDLN3.size(); i++) {
          if (WLDLN3.get(i).contains(appCfg.getCameraData().get(DLNCAM3).getCamWL(DLN).toString()+".")) break;
        }
        if (i<cbDLNc3WL.getItems().size()) cbDLNc3WL.setValue(cbDLNc3WL.getItems().get(i));
      }
      if (appCfg.getCameraData().get(DLNCAM3).getCamWeight(DLN) > 1)
        tfDLNc3W.setText(appCfg.getCameraData().get(DLNCAM3).getCamWeight(DLN).toString());
      else
        tfDLNc3W.clear();
    } else {
      cbDLNc3WL.setValue(cbDLNc3WL.getItems().get(0));
      tfDLNc3W.setDisable(true);
      if (appCfg.getCameraData().get(DLNCAM3).getCamWeight(DLN) > 1)
        tfDLNc3W.setText(appCfg.getCameraData().get(DLNCAM3).getCamWeight(DLN).toString());
      else
        tfDLNc3W.clear();
    }
    if (appCfg.getCameraData().get(DLNCAM3).getCamMaxDataRate() > 0)
      tfDLNc3MDR.setText(appCfg.getCameraData().get(DLNCAM3).getCamMaxDataRate().toString());
    else
      tfDLNc3MDR.clear();

    this.computeConfAndUpdate();
  }
  
  public void onReset() {
    // VBI blue
    cbVBIc1393.setSelected(false);
    tfVBIc1393W.setDisable(true);
    tfVBIc1393W.clear();

    cbVBIc1430.setSelected(false);
    tfVBIc1430W.setDisable(true);
    tfVBIc1430W.clear();

    cbVBIc1450.setSelected(false);
    tfVBIc1450W.setDisable(true);
    tfVBIc1450W.clear();

    cbVBIc1486.setSelected(false);
    tfVBIc1486W.setDisable(true);
    tfVBIc1486W.clear();
    
    tfVBIc1MDR.clear();
    
    // VBI red
    cbVBIc2656.setSelected(false);
    tfVBIc2656W.setDisable(true);
    tfVBIc2656W.clear();

    cbVBIc2668.setSelected(false);
    tfVBIc2668W.setDisable(true);
    tfVBIc2668W.clear();

    cbVBIc2705.setSelected(false);
    tfVBIc2705W.setDisable(true);
    tfVBIc2705W.clear();

    cbVBIc2789.setSelected(false);
    tfVBIc2789W.setDisable(true);
    tfVBIc2789W.clear();

    tfVBIc2MDR.clear();
    
    // ViSP 1
    cbVISPc1WL.getSelectionModel().clearAndSelect(0);
    cbVISPc1WL.getEditor().setText(cbVISPc1WL.getSelectionModel().getSelectedItem().toString());
    tfVISPc1W.clear();
    tfVISPc1W.setDisable(true);
    tfVISPc1MDR.clear();
    
    // ViSP 2
    cbVISPc2WL.getSelectionModel().clearAndSelect(0);
    cbVISPc2WL.getEditor().setText(cbVISPc2WL.getSelectionModel().getSelectedItem().toString());
    tfVISPc2W.clear();
    tfVISPc2W.setDisable(true);
    tfVISPc2MDR.clear();
    
    // ViSP 3
    cbVISPc3WL.getSelectionModel().clearAndSelect(0);
    cbVISPc3WL.getEditor().setText(cbVISPc3WL.getSelectionModel().getSelectedItem().toString());
    tfVISPc3W.clear();
    tfVISPc3W.setDisable(true);
    tfVISPc3MDR.clear();
    
    // VTF
    cbVTFP.setSelected(false);

    cbVTFc1589.setSelected(false);
    tfVTFc1589W.setDisable(true);
    tfVTFc1589W.clear();

    cbVTFc1630.setSelected(false);
    tfVTFc1630W.setDisable(true);
    tfVTFc1630W.clear();

    cbVTFc1656.setSelected(false);
    tfVTFc1656W.setDisable(true);
    tfVTFc1656W.clear();

    cbVTFc1854.setSelected(false);
    tfVTFc1854W.setDisable(true);
    tfVTFc1854W.clear();

    tfVTFc1MDR.clear();
    
     // DLN 1
    cbDLNc1WL.setValue(cbDLNc1WL.getItems().get(0));
    tfDLNc1W.setDisable(true);
    tfDLNc1W.clear();
    tfDLNc1MDR.clear();
    
     // DLN 2
    cbDLNc2WL.setValue(cbDLNc2WL.getItems().get(0));
    tfDLNc2W.setDisable(true);
    tfDLNc2W.clear();
    tfDLNc2MDR.clear();
    
     // DLN 3
    cbDLNc3WL.setValue(cbDLNc3WL.getItems().get(0));
    tfDLNc3W.setDisable(true);
    tfDLNc3W.clear();
    tfDLNc3MDR.clear();

    // reset camera data
    appCfg.resetCameraData();

    // clear best configs list
    bsC.getBestSolutions().clear();

    // clear selection in the ComboBox and select to trigger Listener
    solutions.getItems().clear();
    solutions.getSelectionModel().clearSelection();
    solutions.getSelectionModel().selectFirst();

    this.computeConfAndUpdate();
  }
  
  private void computeConfAndUpdate() {
    bsC.computeMerit(appCfg.getCameraData());
    
    // update ComboBox list items
    solutions.getItems().clear();
    if (bsC.getBestSolutions().size() > 0) {
      solutions.getItems().addAll(bsC.getBestSolutions());
    }
    // update selection in the ComboBox to trigger Listener
    solutions.getSelectionModel().clearSelection();
    solutions.getSelectionModel().selectFirst();
  }
  
  private void updateInfo() {

    ObservableList<String> labelText;
    
    if (solutions.getItems().isEmpty() | bsC.getBestSolutions().isEmpty()) {
      vispLabelOk.setText("n/a");
      vispLabelNo.setText("n/a");
      vbiBLabelOk.setText("n/a");
      vbiBLabelNo.setText("n/a");
      vbiRLabelOk.setText("n/a");
      vbiRLabelNo.setText("n/a");
      vtfLabelOk.setText("n/a");
      vtfLabelNo.setText("n/a");
      dlLabelOk.setText("n/a");
      dlLabelNo.setText("n/a");
      cl2Label.setText("n/a");
      cl2aLabel.setText("n/a");
      cl3Label.setText("n/a");
      cl3aLabel.setText("n/a");
      dataRateLabel.setText("n/a");
      return;
    }
    
    // VBI blue
    labelText = bsC.checkWavelengths(appCfg.getCameraData().get(0), solutions.getSelectionModel().getSelectedIndex());
    if (labelText.size() > 0) {
      vbiBLabelOk.setText(labelText.get(0));
      vbiBLabelNo.setText(labelText.get(1));
    }
    if (vbiBLabelOk.getText().isEmpty()) vbiBLabelOk.setText("n/a");
    if (vbiBLabelNo.getText().isEmpty()) vbiBLabelNo.setText("n/a");
    // set bar diagram fro througput
    pVBIb.setImage(bsC.efficiencyImage(appCfg.getCameraData().get(0), solutions.getSelectionModel().getSelectedIndex()));

    // VBI red
    labelText = bsC.checkWavelengths(appCfg.getCameraData().get(1), solutions.getSelectionModel().getSelectedIndex());
    if (labelText.size() > 0) {
      vbiRLabelOk.setText(labelText.get(0));
      vbiRLabelNo.setText(labelText.get(1));
    }
    if (vbiRLabelOk.getText().isEmpty()) vbiRLabelOk.setText("n/a");
    if (vbiRLabelNo.getText().isEmpty()) vbiRLabelNo.setText("n/a");
    // set bar diagram fro througput
    pVBIr.setImage(bsC.efficiencyImage(appCfg.getCameraData().get(1), solutions.getSelectionModel().getSelectedIndex()));
    
    // ViSP
    labelText = bsC.checkWavelengths(appCfg.getCameraData().get(2), solutions.getSelectionModel().getSelectedIndex());
    if (labelText.size() > 0) {
      vispLabelOk.setText(labelText.get(0));
      vispLabelNo.setText(labelText.get(1));
    }
    labelText = bsC.checkWavelengths(appCfg.getCameraData().get(3), solutions.getSelectionModel().getSelectedIndex());
    if (labelText.size() > 0) {
      vispLabelOk.setText(vispLabelOk.getText()+labelText.get(0));
      vispLabelNo.setText(vispLabelNo.getText()+labelText.get(1));
    }
    labelText = bsC.checkWavelengths(appCfg.getCameraData().get(4), solutions.getSelectionModel().getSelectedIndex());
    if (labelText.size() > 0) {
      vispLabelOk.setText(vispLabelOk.getText()+labelText.get(0));
      vispLabelNo.setText(vispLabelNo.getText()+labelText.get(1));
    }
    if (vispLabelOk.getText().isEmpty()) vispLabelOk.setText("n/a");
    if (vispLabelNo.getText().isEmpty()) vispLabelNo.setText("n/a");
    // set bar diagram for througput (only need one camera, PTI is same for all)
    pVISP.setImage(bsC.efficiencyImage(appCfg.getCameraData().get(2), solutions.getSelectionModel().getSelectedIndex()));

    // VTF
    labelText = bsC.checkWavelengths(appCfg.getCameraData().get(5), solutions.getSelectionModel().getSelectedIndex());
    if (labelText.size() > 0) {
      vtfLabelOk.setText(labelText.get(0));
      vtfLabelNo.setText(labelText.get(1));
    }
    if (vtfLabelOk.getText().isEmpty()) vtfLabelOk.setText("n/a");
    if (vtfLabelNo.getText().isEmpty()) vtfLabelNo.setText("n/a");
    // set bar diagram fro througput
    pVTF.setImage(bsC.efficiencyImage(appCfg.getCameraData().get(5), solutions.getSelectionModel().getSelectedIndex()));
    
    // DL-NIRSP
    labelText = bsC.checkWavelengths(appCfg.getCameraData().get(6), solutions.getSelectionModel().getSelectedIndex());
    if (labelText.size() > 0) {
      dlLabelOk.setText(labelText.get(0));
      dlLabelNo.setText(labelText.get(1));
    }
    labelText = bsC.checkWavelengths(appCfg.getCameraData().get(7), solutions.getSelectionModel().getSelectedIndex());
    if (labelText.size() > 0) {
      dlLabelOk.setText(dlLabelOk.getText()+labelText.get(0));
      dlLabelNo.setText(dlLabelNo.getText()+labelText.get(1));
    }
    labelText = bsC.checkWavelengths(appCfg.getCameraData().get(8), solutions.getSelectionModel().getSelectedIndex());
    if (labelText.size() > 0) {
      dlLabelOk.setText(dlLabelOk.getText()+labelText.get(0));
      dlLabelNo.setText(dlLabelNo.getText()+labelText.get(1));
    }
    if (dlLabelOk.getText().isEmpty()) dlLabelOk.setText("n/a");
    if (dlLabelNo.getText().isEmpty()) dlLabelNo.setText("n/a");
    // set bar diagram for througput (only need one camera, PTI is same for all)
    pDL.setImage(bsC.efficiencyImage(appCfg.getCameraData().get(6), solutions.getSelectionModel().getSelectedIndex()));
    
    labelText = bsC.getOpticName(solutions.getSelectionModel().getSelectedIndex());
    if (labelText.get(0).isEmpty()) cl2Label.setText("n/a"); else cl2Label.setText(labelText.get(0));
    if (labelText.get(1).isEmpty()) cl2aLabel.setText("n/a"); else cl2aLabel.setText(labelText.get(1));
    if (labelText.get(2).isEmpty()) cl3Label.setText("n/a"); else cl3Label.setText(labelText.get(2));
    if (labelText.get(3).isEmpty()) cl3aLabel.setText("n/a"); else cl3aLabel.setText(labelText.get(3));
    
    float dataRate = bsC.checkDataRate(solutions.getSelectionModel().getSelectedIndex(), appCfg.getCameraData());
    if (dataRate > 0.0) {
      if (dataRate > 4000)
        dataRateLabel.setStyle("-fx-text-fill: red;");
      else if (dataRate > 3500)
        dataRateLabel.setStyle("-fx-text-fill: purple;");
      else
        dataRateLabel.setStyle("-fx-text-fill: green;");
      dataRateLabel.setText(Float.toString(dataRate));
    } else dataRateLabel.setText("n/a");
  }
}
