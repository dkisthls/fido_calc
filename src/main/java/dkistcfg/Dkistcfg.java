/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dkistcfg;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import dkistcfg.model.Camera;
import dkistcfg.model.CameraListWrapper;
import dkistcfg.view.CfgToolViewController;
import dkistcfg.view.RootLayoutController;

import java.io.File;
import java.util.prefs.Preferences;
import javafx.application.Platform;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;


/**
 *
 * @author fwoeger
 */
public final class Dkistcfg extends Application {

  private Stage primaryStage;
  private BorderPane rootLayout;
  
  // needs to be public so that Analyze object can have access (RootLayoutController)
  public CfgToolViewController cfgController;

  // need an Analyze object for processing (RootLayoutController)
  public Analyze analyze;

  private final ObservableList<Camera> cameraData = FXCollections.observableArrayList();

  /**
   * on execution of the constructor, reset cameraData
   */
  public Dkistcfg() {
    this.resetCameraData();
  }

  @Override
  public void start(Stage primaryStage) {
    this.primaryStage = primaryStage;
    this.primaryStage.setTitle("DKIST FIDO Configuration Tool");

    initRootLayout();

    showCfgView();
  }

  /**
   * Initializes the root layout.
   */
  public void initRootLayout() {
    try {
      // Load root layout from fxml file.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(this.getClass().getResource("view/RootLayout.fxml"));

      rootLayout = (BorderPane) loader.load();

      // Show the scene containing the root layout.
      Scene scene = new Scene(rootLayout);
      primaryStage.setScene(scene);

      // Give the cfgController access to the main app
      RootLayoutController rootController = loader.getController();
      rootController.setAppCfg(this);

      primaryStage.show();
      
      
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Shows the basic view inside the root layout.
   */
  public void showCfgView() {
    try {
      // Load person overview.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(this.getClass().getResource("view/CfgToolView.fxml"));

      AnchorPane cfgView = (AnchorPane) loader.load();

      // Set basic view into the center of root layout.
      rootLayout.setCenter(cfgView);

      // Give the cfgController access to the main app
      cfgController = loader.getController();
      cfgController.setAppCfg(this);
      
      // at this point it is safe to initialize the Analyze object, as the 
      // BsConfig object has been created in cfgController
      analyze = new Analyze(cfgController.bsC);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
    /**
     * Returns the data as an observable list of Cameras. 
     * @return
     */
    public ObservableList<Camera> getCameraData() {
        return cameraData;
    }

    public void resetCameraData() {
      cameraData.clear();
      /*
       *  setting up Cameras and Path To Instrument with coude optics
       *   array represents {WFC-BS1, CL2, CL2a, CL3, CL3a, CL4}
       *   1: optic reflects
       *  -1: optic transmits
       *   0: optic not relevant for camera
       */
      cameraData.add(0, new Camera("VBI1", new int[]{-1,  1, 1, 0,  0, 0}, 4, 393, 430, 450, 486)); //VBI blue
      cameraData.add(1, new Camera("VBI2", new int[]{-1, -1, 0, 1, -1, 0}, 4, 656, 668, 705, 789)); //VBI red
      cameraData.add(2, new Camera("VISP1", new int[]{-1, 1, -1, 0, 0, 0}, 1, 0)); //VISP arm 1
      cameraData.add(3, new Camera("VISP2", new int[]{-1, 1, -1, 0, 0, 0}, 1, 0)); //VISP arm 2
      cameraData.add(4, new Camera("VISP3", new int[]{-1, 1, -1, 0, 0, 0}, 1, 0)); //VISP arm 3
      cameraData.add(5, new Camera("VTF1", new int[]{-1, -1, 0,  1, 1, 0}, 4, 589, 630, 656, 854)); // VTF
      cameraData.add(6, new Camera("DLN1", new int[]{-1, -1, 0, -1, 0, -1}, 1, 0)); //DL arm 1
      cameraData.add(7, new Camera("DLN2", new int[]{-1, -1, 0, -1, 0, -1}, 1, 0)); //DL arm 2
      cameraData.add(8, new Camera("DLN3", new int[]{-1, -1, 0, -1, 0, -1}, 1, 0)); //DL arm 3
    }
    
    public CfgToolViewController getController() {
      return cfgController;
    }
    
  /**
   * Returns the person file preference, i.e. the file that was last opened.
   * The preference is read from the OS specific registry. If no such
   * preference can be found, null is returned.
   * 
   * @return
   */
  public File getCameraFilePath() {
      Preferences prefs = Preferences.userNodeForPackage(Dkistcfg.class);
      String filePath = prefs.get("filePath", null);
      if (filePath != null) {
          return new File(filePath);
      } else {
          return null;
      }
  }

  /**
   * Sets the file path of the currently loaded file. The path is persisted in
   * the OS specific registry.
   * 
   * @param file the file or null to remove the path
   */
  public void setCameraFilePath(File file) {
      Preferences prefs = Preferences.userNodeForPackage(Dkistcfg.class);
      if (file != null) {
          prefs.put("filePath", file.getPath());

          // Update the stage title.
          primaryStage.setTitle("DKIST FIDO Configuration Tool - " + file.getName());
      } else {
          prefs.remove("filePath");

          // Update the stage title.
          primaryStage.setTitle("DKIST FIDO Configuration Tool");
      }
  }  
  
  /**
   * Loads camera data from the specified file. The current camera data will
   * be replaced.
   * 
   * @param file
   */
  public void loadCameraDataFromFile(File file) {
      try {
          JAXBContext context = JAXBContext
                  .newInstance(CameraListWrapper.class);
          Unmarshaller um = context.createUnmarshaller();

          // Reading XML from the file and unmarshalling.
          CameraListWrapper wrapper = (CameraListWrapper) um.unmarshal(file);

          cameraData.clear();
          cameraData.addAll(wrapper.getCameras());

          cfgController.onLoad();
      } catch (JAXBException e) { // catches ANY exception
          Alert alert = new Alert(AlertType.ERROR);
          alert.setTitle("Error");
          alert.setHeaderText("Could not load data");
          alert.setContentText("Could not load data from file:\n" + file.getPath());
          alert.setResizable(true);
          alert.onShownProperty().addListener(ll -> { 
            Platform.runLater(() -> alert.setResizable(false)); 
          });
          e.printStackTrace();

          alert.showAndWait();
      }
  }

  /**
   * Saves the current camera data to the specified file.
   * 
   * @param file
   */
  public void saveCameraDataToFile(File file) {
      try {
          JAXBContext context = JAXBContext
                  .newInstance(CameraListWrapper.class);
          Marshaller m = context.createMarshaller();
          m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

          // Wrapping our person data.
          CameraListWrapper wrapper = new CameraListWrapper();
          wrapper.setCameras(cameraData);

          // Marshalling and saving XML to the file.
          m.marshal(wrapper, file);

      } catch (JAXBException e) { // catches ANY exception
          Alert alert = new Alert(AlertType.ERROR);
          alert.setTitle("Error");
          alert.setHeaderText("Could not save data");
          alert.setContentText("Could not save data to file:\n" + file.getPath());
          alert.setResizable(true);
          alert.onShownProperty().addListener(ll -> { 
            Platform.runLater(() -> alert.setResizable(false)); 
          });
          e.printStackTrace();

          alert.showAndWait();
      }
  }

  /**
   * Returns the main stage.
   * @return
   */
  public Stage getPrimaryStage() {
    return primaryStage;
  }

  public static void main(String[] args) {
    launch(args);
  } 
}
