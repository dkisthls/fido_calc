/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dkistcfg.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;

/**
 *
 * @author fwoeger
 */
public class Camera {
  public static final int DEFAULTWEIGHT = 1;
  public static final float DEFAULTDATARATE = 0;
  
  private String name;
  private List<Integer> wavelengths;
  private List<Integer> wavelengthsidx;
  private List<Boolean> enabled;
  private List<Integer> weight;
  private Float maxDataRate;
  private Boolean modifierFlag;
  private List<Integer> pathToInstrument;

  public Camera() {
    this("none", new int[]{0, 0, 0, 0, 0, 0}, 0);
  }

  public Camera(String camName, int[] pti, int numWL, int... wavel) {
    this.name = camName;

    this.wavelengths = new ArrayList<>();
    for (int i=0; i<numWL; i++) this.wavelengths.add(wavel[i]);

    this.wavelengthsidx = new ArrayList<>();
    for (int i=0; i<numWL; i++) this.wavelengthsidx.add(-1);

    this.enabled = new ArrayList<>();
    for (int i=0; i<numWL; i++) this.enabled.add(false);

    this.weight = new ArrayList<>();
    for (int i=0; i<numWL; i++) this.weight.add(Camera.DEFAULTWEIGHT);

    this.maxDataRate = Camera.DEFAULTDATARATE;

    this.modifierFlag = false;

    this.pathToInstrument = new ArrayList<>();
    for (int i=0; i<pti.length; i++) this.pathToInstrument.add(pti[i]);
  }

  /**
   * for JAXB
   * @return 
   */
  // Camera Name
  @XmlElement(name = "CamName")
  public String getCamName() {
    return name;
  }
  public void setCamName(String camName) {
    this.name = camName;
  }

  // Camera wavelengths
  @XmlElement(name = "CamWavelengths")
  public List<Integer> getCamWLList() {
    return wavelengths;
  }
  public void setCamWLList(List<Integer> wavelengths) {
    this.wavelengths = wavelengths;
  }

  // Camera wavelengths indices
  @XmlElement(name = "CamWavelengthsIdxs")
  public List<Integer> getCamWLidxList() {
    return wavelengthsidx;
  }
  public void setCamWLidxList(List<Integer> wavelengthsidx) {
    this.wavelengthsidx = wavelengthsidx;
  }

  // Camera enabled wavelengths
  @XmlElement(name = "CamEnabledWavelengths")
  public List<Boolean> getCamEnabledList() {
    return enabled;
  }
  public void setCamEnabledList(List<Boolean> enabled) {
    this.enabled = enabled;
  }

  // Camera wavelength weights
  @XmlElement(name = "CamWavelengthsWeights")
  public List<Integer> getCamWeightList() {
    return weight;
  }
  public void setCamWeightList(List<Integer> weight) {
    this.weight = weight;
  }

  // Camera maximum data rate
  @XmlElement(name = "CamMaxDataRate")
  public Float getCamMaxDataRate() {
    return maxDataRate;
  }
  public void setCamMaxDataRate(Float camMaxDataRate) {
    this.maxDataRate = camMaxDataRate;
  }

  // Camera extra modifier flag
  @XmlElement(name = "CamModifierFlag")
  public boolean getCamModFlag() {
    return modifierFlag;
  }
  public void setCamModFlag(boolean modFlag) {
    this.modifierFlag = modFlag;
  }

  // Camera descriptors for path to instrument (coude optics)
  @XmlElement(name = "CamPathToInstrument")
  public List<Integer> getCamPTIList() {
    return pathToInstrument;
  }
  public void setCamPTIList(List<Integer> pathToInstrument) {
    this.pathToInstrument = pathToInstrument;
  }

  /** 
   * for other things
   * @return 
   **/
  // Camera wavelengths
  public Integer getCamWLsize() {
    return this.wavelengths.size();
  }
  public Integer getCamWL(int i) {
    return wavelengths.get(i);
  }
  public void setCamWL(int i, int wl) {
    this.wavelengths.set(i, wl);
  }

  // Camera wavelengths indices
  public Integer getCamWLidxsize() {
    return this.wavelengthsidx.size();
  }
  public Integer getCamWLidx(int i) {
    return wavelengthsidx.get(i);
  }
  public void setCamWLidx(int i, int idx) {
    this.wavelengthsidx.set(i, idx);
  }

  // Camera enabled wavelengths
  public Integer getCamEnabledsize() {
    return this.enabled.size();
  }
  public Boolean getCamEnabled(int i) {
    return enabled.get(i);
  }
  public void setCamEnabled(int i, boolean enabled) {
    this.enabled.set(i, enabled);
  }

  // Camera wavelength weights
  public Integer getCamWeightsize() {
    return this.weight.size();
  }
  public Integer getCamWeight(int i) {
    return weight.get(i);
  }
  public void setCamWeight(int i, int weight) {
    this.weight.set(i, weight);
  }

  // Camera descriptors for path to instrument (coude optics)
  public Integer getCamPTIsize() {
    return this.pathToInstrument.size();
  }
  public Integer getCamPTI(int i) {
    return pathToInstrument.get(i);
  }
  public void setCamPTI(int i, int reflection) {
    this.pathToInstrument.set(i, reflection);
  }
}
