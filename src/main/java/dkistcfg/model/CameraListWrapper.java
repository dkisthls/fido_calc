/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dkistcfg.model;

import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fwoeger
 */
@XmlRootElement(name = "cameras")
public class CameraListWrapper {
  private List<Camera> cameras;
  
  @XmlElement(name = "camera")
  public List<Camera> getCameras() {
    return cameras;
  }
  
  public void setCameras(List<Camera> cameras) {
    this.cameras = cameras;
  }
}
