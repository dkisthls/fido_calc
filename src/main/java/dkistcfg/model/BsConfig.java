/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dkistcfg.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.PixelBuffer;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;

/**
 *
 * @author fwoeger
 */
public class BsConfig {

  // miniml threshold to be viable
  public final static double MINTHRESH = 0.8;
  
  // Set expected character encoding in file.
  final static Charset ENCODING = StandardCharsets.UTF_8;

  final static ArrayList<Double> wlcWFCBS1 = new ArrayList<>();
  final static ArrayList<Double> cWFCBS1 = new ArrayList<>();
  
  final static ArrayList<Double> wlcBS465 = new ArrayList<>();
  final static ArrayList<Double> cBS465 = new ArrayList<>();
  
  final static ArrayList<Double> wlcBS555 = new ArrayList<>();
  final static ArrayList<Double> cBS555 = new ArrayList<>();
  
  final static ArrayList<Double> wlcBS643 = new ArrayList<>();
  final static ArrayList<Double> cBS643 = new ArrayList<>();
  
  final static ArrayList<Double> wlcBS680 = new ArrayList<>();
  final static ArrayList<Double> cBS680 = new ArrayList<>();
  
  final static ArrayList<Double> wlcBS950 = new ArrayList<>();
  final static ArrayList<Double> cBS950 = new ArrayList<>();
  
  final static ArrayList<Double> wlcW1 = new ArrayList<>();
  final static ArrayList<Double> cW1 = new ArrayList<>();
  
  final static ArrayList<Double> wlcW2 = new ArrayList<>();
  final static ArrayList<Double> cW2 = new ArrayList<>();
  
  final static ArrayList<Double> wlcW3 = new ArrayList<>();
  final static ArrayList<Double> cW3 = new ArrayList<>();
  
  final static ArrayList<Double> wlcM1 = new ArrayList<>();
  final static ArrayList<Double> cM1 = new ArrayList<>();

  final static ArrayList<ArrayList<Double>> track = new ArrayList<>();
  
  public ObservableList<ArrayList<Integer>> bestConfigs = FXCollections.observableArrayList();

  public BsConfig() {
    try {

/***** if one needs to read multi-column files, and some columns aren't not needed *****
 *    ArrayList<Double> trash = new ArrayList<>();
 *    this.readData(this.getClass().getResourceAsStream("reflectance_465_aoi15.txt"),
 *        BsConfig.wlcBS465, BsConfig.cBS465, trash, trash
 *    );
 *    trash.clear();
 */
      // Dichroics
      this.readData(
          this.getClass().getResourceAsStream("cbs465.txt"),
          BsConfig.wlcBS465, BsConfig.cBS465
      );
      this.readData(
          this.getClass().getResourceAsStream("cbs555.txt"),
          BsConfig.wlcBS555, BsConfig.cBS555
      );
      this.readData(
          this.getClass().getResourceAsStream("cbs643.txt"),
          BsConfig.wlcBS643, BsConfig.cBS643
      );
      this.readData(
          this.getClass().getResourceAsStream("cbs680.txt"),
          BsConfig.wlcBS680, BsConfig.cBS680
      );
      this.readData(
          this.getClass().getResourceAsStream("cbs950.txt"),
          BsConfig.wlcBS950, BsConfig.cBS950
      );
      // WFC-BS1
      this.readData(
          this.getClass().getResourceAsStream("fresnel.txt"),
          this.wlcWFCBS1, this.cWFCBS1
      );
      // Window 1
      this.readData(
          this.getClass().getResourceAsStream("wbbar1.txt"),
          this.wlcW1, this.cW1
      );
      // Window 2 (copy of WFC-BS1, Fresnel reflection off the front surface)
      for (int i=0; i < this.wlcBS465.size(); i++) {
        this.wlcW2.add(i, this.wlcBS465.get(i));
        this.cW2.add(i, this.cWFCBS1.get(i));
      }
      // Window 3 (copy of W1)
      for (int i=0; i < this.wlcW1.size(); i++) {
        this.wlcW3.add(i, this.wlcW1.get(i));
        this.cW3.add(i, this.cW1.get(i));
      }
      // Mirror 1
      this.readData(
          this.getClass().getResourceAsStream("cm1.txt"),
          this.wlcM1, this.cM1
      );
    } catch(IOException error) {
      System.out.println(Arrays.toString(error.getStackTrace()));
    }
    
    // setup tracking list, this, in particular the order, is HIGHLY important
    this.track.add(cBS465);
    this.track.add(cBS555);
    this.track.add(cBS643);
    this.track.add(cBS680);
    this.track.add(cBS950);
    this.track.add(cW1);
    this.track.add(cW2);
    this.track.add(cM1);

  }
  
  private void readData(InputStream fileStream, ArrayList<Double>... vars) throws IOException {

    BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream, ENCODING));
    int outputArrayNumber = vars.length;

    double newVal;

    try {
      String line;
      while ((line = reader.readLine()) != null) {
        String[] columns = line.split("\\s+");

        // If the first column is preceded by white spaces, then we have one
        // column too many (the first one being bogus), so we have to watch out:
        int offset;
        if (columns[0].matches("^\\s*$")) {offset = 1;} else {offset = 0;}

        // If there aren't as many values as input arrays, we can skip the line
        if ((columns.length-offset) != outputArrayNumber) { continue; }

        for (int i=0; i<outputArrayNumber; i++) {
          try {
            newVal = Double.parseDouble(columns[i + offset]);
          } catch (NumberFormatException e1) {
            continue;
          }
          vars[i].add(newVal);
        }
      }
    } catch (IOException e2) {
        System.out.println(Arrays.toString(e2.getStackTrace()));
    } finally {
      try {
        reader.close();
      } catch (IOException e3) {
          System.out.println(Arrays.toString(e3.getStackTrace()));
      }
    }
  }
  
  public ArrayList<Double> getWLOptic(String which) {
    switch (which) {
      case "wlcwfcbs1": return BsConfig.wlcWFCBS1;
      case "wlcbs465": return BsConfig.wlcBS465;
      case "wlcbs555": return BsConfig.wlcBS555;
      case "wlcbs643": return BsConfig.wlcBS643;
      case "wlcbs680": return BsConfig.wlcBS680;
      case "wlcbs950": return BsConfig.wlcBS950;
      case "wlcw1": return BsConfig.wlcW1;
      case "wlcw2": return BsConfig.wlcW2;
      case "wlcw3": return BsConfig.wlcW3;
      case "wlcm1": return BsConfig.wlcM1;
    }
    return null;
  }
  
  public ArrayList<Double> getOptic(String which) {
    switch (which) {
      case "cwfcbs1": return BsConfig.cWFCBS1;
      case "cbs465": return BsConfig.cBS465;
      case "cbs555": return BsConfig.cBS555;
      case "cbs643": return BsConfig.cBS643;
      case "cbs680": return BsConfig.cBS680;
      case "cbs950": return BsConfig.cBS950;
      case "cw1": return BsConfig.cW1;
      case "cw2": return BsConfig.cW2;
      case "cw3": return BsConfig.cW3;
      case "cm1": return BsConfig.cM1;
    }
    return null;
  }
  
  public ObservableList<String> getOpticName(int bsConfIdx) {
    ObservableList<String> output = FXCollections.observableArrayList(new ArrayList<>());

    ArrayList<Integer> bsConf = (ArrayList<Integer>) this.getBestSolutions().get(bsConfIdx);

    for (Integer bsConf1 : bsConf) {output.add("");}

    for (int i=0; i<bsConf.size(); i++) {
      // index has to match track variable index
      switch (bsConf.get(i)) {
        case 0:
          output.set(i, "C-BS465");
          break;
        case 1:
          output.set(i, "C-BS555");
          break;
        case 2:
          output.set(i, "C-BS643");
          break;
        case 3:
          output.set(i, "C-BS680");
          break;
        case 4:
          output.set(i, "C-BS950");
          break;
        case 5:
          output.set(i, "C-W1");
          break;
        case 6:
          output.set(i, "C-W2");
          break;
        case 7:
          output.set(i, "C-M1");
          break;
        default:
          output.set(i, "n/a");
      }
    }

    return output;
  }

  public int computeMerit(ObservableList<Camera> cams) {
    int globalMerit = 0;
    this.bestConfigs.clear();
    
    // find index of requested wavelength in wavelength vector
    for (Camera cam : cams) {
      for (int k = 0; k < cam.getCamWLsize(); k++) {
        if (cam.getCamEnabled(k)) {
          int j;
          for (j = 0; j<wlcBS465.size(); j++) {
            if (this.wlcBS465.get(j).intValue() == cam.getCamWL(k)) {
              break;
            }
          }
          // ViSP could throw an error, so have to check
          if (j<wlcBS465.size()) cam.setCamWLidx(k, j); else cam.setCamWLidx(k, -1);
        } else {
          cam.setCamWLidx(k, -1);
        }
      }
    }

    for (int cl2=0; cl2<this.track.size(); cl2++) {
      for (int cl2a=0; cl2a<this.track.size(); cl2a++) {
        if (cl2a!=cl2) {
          for (int cl3=0; cl3<this.track.size(); cl3++) {
            if ((cl3!=cl2a) & (cl3!=cl2)) {
              for (int cl3a=0; cl3a<this.track.size(); cl3a++) {
                if ((cl3a!=cl3) & (cl3a!=cl2a) & (cl3a!=cl2)) {
                  int localMerit = 0;
                  for (Camera cam : cams) {
                    for (int i=0; i<cam.getCamWLidxsize(); i++) {
                      double throughput = 1.0;
                      // check if cam is active and ignore the -1 indices
                      if (cam.getCamEnabled(i)) {
                        if (cam.getCamWLidx(i) > 0) {
                          /* the polynomial below makes sure that the following
                             points are hit: (-1,1), (0,1), (1,0)
                             this allows to create the following:
                             1. when -1 is indicated in the PTI array, the
                                transmission factor is calculated as 1-reflectivity
                             2. when  1 is indicated in the PTI array, the
                                reflectivity factor is returned
                             3. when  0 is indicated in the PTI array, the
                                factor is set to 1
                          */
                          throughput *=
                            ((-0.5*(cam.getCamPTI(0)*cam.getCamPTI(0))-0.5*cam.getCamPTI(0)+1)
                             +cam.getCamPTI(0)*cWFCBS1.get(cam.getCamWLidx(i))/100.)
                           *((-0.5*(cam.getCamPTI(1)*cam.getCamPTI(1))-0.5*cam.getCamPTI(1)+1)
                             +cam.getCamPTI(1)*this.track.get(cl2).get(cam.getCamWLidx(i))/100.)
                           *((-0.5*(cam.getCamPTI(2)*cam.getCamPTI(2))-0.5*cam.getCamPTI(2)+1)
                             +cam.getCamPTI(2)*this.track.get(cl2a).get(cam.getCamWLidx(i))/100.)
                           *((-0.5*(cam.getCamPTI(3)*cam.getCamPTI(3))-0.5*cam.getCamPTI(3)+1)
                             +cam.getCamPTI(3)*this.track.get(cl3).get(cam.getCamWLidx(i))/100.)
                           *((-0.5*(cam.getCamPTI(4)*cam.getCamPTI(4))-0.5*cam.getCamPTI(4)+1)
                             +cam.getCamPTI(4)*this.track.get(cl3a).get(cam.getCamWLidx(i))/100.)
                           *((-0.5*(cam.getCamPTI(5)*cam.getCamPTI(5))-0.5*cam.getCamPTI(5)+1)
                             +cam.getCamPTI(5)*cW3.get(cam.getCamWLidx(i))/100.);
                          if (throughput >= MINTHRESH) localMerit += cam.getCamWeight(i);
                        }
                      }
                    }
                  }
                  if (localMerit > 0) {
                    //System.out.println(localMerit + " - " + globalMerit);
                    if (localMerit > globalMerit) {
                      globalMerit = localMerit;
                      this.bestConfigs.clear();
                      this.bestConfigs.add(new ArrayList<>());
                      this.bestConfigs.get(0).add(cl2);
                      this.bestConfigs.get(0).add(cl2a);
                      this.bestConfigs.get(0).add(cl3);
                      this.bestConfigs.get(0).add(cl3a);
                      //System.out.println("better: " + cl2 + ";" + cl2a + ";" + cl3 + ";" + cl3a);
                    } else if (localMerit == globalMerit) {
                      this.bestConfigs.add(new ArrayList<>());
                      this.bestConfigs.get(this.bestConfigs.size()-1).add(cl2);
                      this.bestConfigs.get(this.bestConfigs.size()-1).add(cl2a);
                      this.bestConfigs.get(this.bestConfigs.size()-1).add(cl3);
                      this.bestConfigs.get(this.bestConfigs.size()-1).add(cl3a);
                      //System.out.println("same");
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    /*
    System.out.println(this.bestConfigs.size());
    System.out.println(this.bestConfigs.get(this.bestConfigs.size()-1).get(0) + "-"
                      +this.bestConfigs.get(this.bestConfigs.size()-1).get(1) + "-"
                      +this.bestConfigs.get(this.bestConfigs.size()-1).get(2) + "-"
                      +this.bestConfigs.get(this.bestConfigs.size()-1).get(3)
    );
    */
    checkEquivalentConfigs(cams);
    //System.out.println(this.bestConfigs.size());

    return 0;
  }
  
  // remove equivalent configurations, favor those that have the dichroics
  private void checkEquivalentConfigs(ObservableList<Camera> cams) {
    
    ArrayList<Integer> picked;
    ArrayList<Integer> current;
    
    if (this.bestConfigs.size() > 0) {
      for (int i=0; i<this.bestConfigs.size(); i++) {
        picked = this.bestConfigs.get(i);

        for (int j=i+1; j<this.bestConfigs.size(); j++) {
          boolean sameFlag = true;
          current = this.bestConfigs.get(j);
          for (Camera cam : cams) {
            for (int k=0; k<cam.getCamWLidxsize(); k++) {
              // check if cam is active and ignore the -1 indices
              if (cam.getCamEnabled(k)) {
                if (cam.getCamWLidx(k) > 0) {
                  double tpP = 1.0;
                  double tpC = 1.0;
                  /* the polynomial below makes sure that the following
                     points are hit: (-1,1), (0,1), (1,0)
                     this allows to create the following:
                     1. when -1 is indicated in the PTI array, the
                        transmission factor is calculated as 1-reflectivity
                     2. when  1 is indicated in the PTI array, the
                        reflectivity factor is returned
                     3. when  0 is indicated in the PTI array, the
                        factor is set to 1
                  */
                  tpP *=
                    ((-0.5*(cam.getCamPTI(0)*cam.getCamPTI(0))-0.5*cam.getCamPTI(0)+1)
                     +cam.getCamPTI(0)*cWFCBS1.get(cam.getCamWLidx(k))/100.)
                   *((-0.5*(cam.getCamPTI(1)*cam.getCamPTI(1))-0.5*cam.getCamPTI(1)+1)
                     +cam.getCamPTI(1)*this.track.get(picked.get(0)).get(cam.getCamWLidx(k))/100.)
                   *((-0.5*(cam.getCamPTI(2)*cam.getCamPTI(2))-0.5*cam.getCamPTI(2)+1)
                     +cam.getCamPTI(2)*this.track.get(picked.get(1)).get(cam.getCamWLidx(k))/100.)
                   *((-0.5*(cam.getCamPTI(3)*cam.getCamPTI(3))-0.5*cam.getCamPTI(3)+1)
                     +cam.getCamPTI(3)*this.track.get(picked.get(2)).get(cam.getCamWLidx(k))/100.)
                   *((-0.5*(cam.getCamPTI(4)*cam.getCamPTI(4))-0.5*cam.getCamPTI(4)+1)
                     +cam.getCamPTI(4)*this.track.get(picked.get(3)).get(cam.getCamWLidx(k))/100.)
                   *((-0.5*(cam.getCamPTI(5)*cam.getCamPTI(5))-0.5*cam.getCamPTI(5)+1)
                     +cam.getCamPTI(5)*cW3.get(cam.getCamWLidx(k))/100.);

                  tpC *=
                    ((-0.5*(cam.getCamPTI(0)*cam.getCamPTI(0))-0.5*cam.getCamPTI(0)+1)
                     +cam.getCamPTI(0)*cWFCBS1.get(cam.getCamWLidx(k))/100.)
                   *((-0.5*(cam.getCamPTI(1)*cam.getCamPTI(1))-0.5*cam.getCamPTI(1)+1)
                     +cam.getCamPTI(1)*this.track.get(current.get(0)).get(cam.getCamWLidx(k))/100.)
                   *((-0.5*(cam.getCamPTI(2)*cam.getCamPTI(2))-0.5*cam.getCamPTI(2)+1)
                     +cam.getCamPTI(2)*this.track.get(current.get(1)).get(cam.getCamWLidx(k))/100.)
                   *((-0.5*(cam.getCamPTI(3)*cam.getCamPTI(3))-0.5*cam.getCamPTI(3)+1)
                     +cam.getCamPTI(3)*this.track.get(current.get(2)).get(cam.getCamWLidx(k))/100.)
                   *((-0.5*(cam.getCamPTI(4)*cam.getCamPTI(4))-0.5*cam.getCamPTI(4)+1)
                     +cam.getCamPTI(4)*this.track.get(current.get(3)).get(cam.getCamWLidx(k))/100.)
                   *((-0.5*(cam.getCamPTI(5)*cam.getCamPTI(5))-0.5*cam.getCamPTI(5)+1)
                     +cam.getCamPTI(5)*cW3.get(cam.getCamWLidx(k))/100.);

                  if ( ((tpP >= MINTHRESH) & (tpC < MINTHRESH))  | 
                       ((tpP < MINTHRESH)  & (tpC >= MINTHRESH)) ) {
                    sameFlag = false;
                  }
                }
              }
              if (!sameFlag) break;
            }
            if (!sameFlag) break;
          }
          // minimize the cross total; the way that 'track' was set up, we will
          // favor dichroic configurations over window/mirror configurations
          if (sameFlag) {
            int csum = current.get(0) + current.get(1) + current.get(2) + current.get(3);
            int psum = picked.get(0) + picked.get(1) + picked.get(2) + picked.get(3);
            if (csum >= psum) {
              this.bestConfigs.remove(current);
            } else {
              this.bestConfigs.remove(picked);
              break;
            }
          }
        }
      }
    }
  }

  public ObservableList<String> checkWavelengths(Camera cam, Integer bsConfIdx) {
    ObservableList<String> output = FXCollections.observableArrayList(new ArrayList<>());

    ArrayList<Integer> bsConf = (ArrayList<Integer>) this.getBestSolutions().get(bsConfIdx);

    String ok = "";
    String no = "";

    for (int k=0; k<cam.getCamWLidxsize(); k++) {
      // check if cam is active and ignore the -1 indices
      if (cam.getCamEnabled(k)) {
        if (cam.getCamWLidx(k) > 0) {
          double tH = 1.0;
          /* the polynomial below makes sure that the following
             points are hit: (-1,1), (0,1), (1,0)
             this allows to create the following:
             1. when -1 is indicated in the PTI array, the
                transmission factor is calculated as 1-reflectivity
             2. when  1 is indicated in the PTI array, the
                reflectivity factor is returned
             3. when  0 is indicated in the PTI array, the
                factor is set to 1
          */
          tH *=
            ((-0.5*(cam.getCamPTI(0)*cam.getCamPTI(0))-0.5*cam.getCamPTI(0)+1)
             +cam.getCamPTI(0)*cWFCBS1.get(cam.getCamWLidx(k))/100.)
           *((-0.5*(cam.getCamPTI(1)*cam.getCamPTI(1))-0.5*cam.getCamPTI(1)+1)
             +cam.getCamPTI(1)*this.track.get(bsConf.get(0)).get(cam.getCamWLidx(k))/100.)
           *((-0.5*(cam.getCamPTI(2)*cam.getCamPTI(2))-0.5*cam.getCamPTI(2)+1)
             +cam.getCamPTI(2)*this.track.get(bsConf.get(1)).get(cam.getCamWLidx(k))/100.)
           *((-0.5*(cam.getCamPTI(3)*cam.getCamPTI(3))-0.5*cam.getCamPTI(3)+1)
             +cam.getCamPTI(3)*this.track.get(bsConf.get(2)).get(cam.getCamWLidx(k))/100.)
           *((-0.5*(cam.getCamPTI(4)*cam.getCamPTI(4))-0.5*cam.getCamPTI(4)+1)
             +cam.getCamPTI(4)*this.track.get(bsConf.get(3)).get(cam.getCamWLidx(k))/100.)
           *((-0.5*(cam.getCamPTI(5)*cam.getCamPTI(5))-0.5*cam.getCamPTI(5)+1)
             +cam.getCamPTI(5)*cW3.get(cam.getCamWLidx(k))/100.);

          if (tH >= BsConfig.MINTHRESH) {
            ok += cam.getCamWL(k).toString() + "/" + String.valueOf(Math.round(tH*100.)) + "% ";
          } else {
            no += cam.getCamWL(k).toString() + "/" + String.valueOf(Math.round(tH*100.)) + "% ";
          }
        }
      }
    }
    output.add(ok);
    output.add(no);

    return output;
  }

  public Image efficiencyImage(Camera cam, Integer bsConfIdx) {
    int maxRows = 2;  
    
    int sizeWl = wlcBS465.size();
    
    int[] buffer = new int[ sizeWl * maxRows ];

    ArrayList<Integer> bsConf = (ArrayList<Integer>) this.getBestSolutions().get(bsConfIdx);

    for (int k=0; k<sizeWl; k++) {
      double tH = 1.0;
      /* the polynomial below makes sure that the following
         points are hit: (-1,1), (0,1), (1,0)
         this allows to create the following:
         1. when -1 is indicated in the PTI array, the
            transmission factor is calculated as 1-reflectivity
         2. when  1 is indicated in the PTI array, the
            reflectivity factor is returned
         3. when  0 is indicated in the PTI array, the
            factor is set to 1
      */
      tH *=
        ((-0.5*(cam.getCamPTI(0)*cam.getCamPTI(0))-0.5*cam.getCamPTI(0)+1.0)
         +cam.getCamPTI(0)*cWFCBS1.get(k)/100.)
       *((-0.5*(cam.getCamPTI(1)*cam.getCamPTI(1))-0.5*cam.getCamPTI(1)+1.0)
         +cam.getCamPTI(1)*this.track.get(bsConf.get(0)).get(k)/100.)
       *((-0.5*(cam.getCamPTI(2)*cam.getCamPTI(2))-0.5*cam.getCamPTI(2)+1.0)
         +cam.getCamPTI(2)*this.track.get(bsConf.get(1)).get(k)/100.)
       *((-0.5*(cam.getCamPTI(3)*cam.getCamPTI(3))-0.5*cam.getCamPTI(3)+1.0)
         +cam.getCamPTI(3)*this.track.get(bsConf.get(2)).get(k)/100.)
       *((-0.5*(cam.getCamPTI(4)*cam.getCamPTI(4))-0.5*cam.getCamPTI(4)+1.0)
         +cam.getCamPTI(4)*this.track.get(bsConf.get(3)).get(k)/100.)
       *((-0.5*(cam.getCamPTI(5)*cam.getCamPTI(5))-0.5*cam.getCamPTI(5)+1.0)
         +cam.getCamPTI(5)*cW3.get(k)/100.);
      
      // set ARGB pixels
      byte[] temp = new byte[] {(byte)255, (byte)(tH*255), (byte)(tH*255), (byte)(tH*255)};
      
      buffer[k] = ByteBuffer.wrap(temp).getInt();
    }
    // copy
    for (int row=1; row<maxRows; row++) {
        System.arraycopy(buffer, 0, buffer, row*sizeWl, sizeWl);
    }

    // convert a byte[] to a JavaFX image (what a pain)
    IntBuffer intBuffer = IntBuffer.wrap(buffer);
    PixelFormat<IntBuffer> pixelFormat = PixelFormat.getIntArgbPreInstance();
    PixelBuffer<IntBuffer> pixelBuffer = new PixelBuffer<>(sizeWl, maxRows, intBuffer, pixelFormat);

    Image output = new WritableImage(pixelBuffer);
 
    return output;
  }

  
  public float checkDataRate(Integer bsConfIdx, ObservableList<Camera> cams) {
    float output = 0;

    ArrayList<Integer> bsConf = (ArrayList<Integer>) this.getBestSolutions().get(bsConfIdx);
    
    for (Camera cam : cams) {
      boolean usedFlag = false;
      for (int k=0; k<cam.getCamWLidxsize(); k++) {
        if (cam.getCamEnabled(k)) {
          if (cam.getCamWLidx(k) > 0) {
            double tH = 1.0;
            /* the polynomial below makes sure that the following
               points are hit: (-1,1), (0,1), (1,0)
               this allows to create the following:
               1. when -1 is indicated in the PTI array, the
                  transmission factor is calculated as 1-reflectivity
               2. when  1 is indicated in the PTI array, the
                  reflectivity factor is returned
               3. when  0 is indicated in the PTI array, the
                  factor is set to 1
            */
            tH *=
              ((-0.5*(cam.getCamPTI(0)*cam.getCamPTI(0))-0.5*cam.getCamPTI(0)+1)
               +cam.getCamPTI(0)*cWFCBS1.get(cam.getCamWLidx(k))/100.)
             *((-0.5*(cam.getCamPTI(1)*cam.getCamPTI(1))-0.5*cam.getCamPTI(1)+1)
               +cam.getCamPTI(1)*this.track.get(bsConf.get(0)).get(cam.getCamWLidx(k))/100.)
             *((-0.5*(cam.getCamPTI(2)*cam.getCamPTI(2))-0.5*cam.getCamPTI(2)+1)
               +cam.getCamPTI(2)*this.track.get(bsConf.get(1)).get(cam.getCamWLidx(k))/100.)
             *((-0.5*(cam.getCamPTI(3)*cam.getCamPTI(3))-0.5*cam.getCamPTI(3)+1)
               +cam.getCamPTI(3)*this.track.get(bsConf.get(2)).get(cam.getCamWLidx(k))/100.)
             *((-0.5*(cam.getCamPTI(4)*cam.getCamPTI(4))-0.5*cam.getCamPTI(4)+1)
               +cam.getCamPTI(4)*this.track.get(bsConf.get(3)).get(cam.getCamWLidx(k))/100.)
             *((-0.5*(cam.getCamPTI(5)*cam.getCamPTI(5))-0.5*cam.getCamPTI(5)+1)
               +cam.getCamPTI(5)*cW3.get(cam.getCamWLidx(k))/100.);

            if (tH >= BsConfig.MINTHRESH)
              usedFlag = true;
            else 
              usedFlag = usedFlag ? true : false;
          } else usedFlag = usedFlag ? true : false;
        } else usedFlag = usedFlag ? true : false;
      }
      if (usedFlag) {
        // VTF is spcial here
        if (cam.getCamName().contains("VTF1")) {
          output += cam.getCamModFlag() ? 3*cam.getCamMaxDataRate() : 2*cam.getCamMaxDataRate();
        } else 
          output += cam.getCamMaxDataRate();
      }
        
    }
    return output;
  }
  
  
  public ObservableList getBestSolutions() {
    return this.bestConfigs;
  }
}
