/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dkistcfg;

/**
 *
 * @author fwoeger
 * this is needed because there are some issues with OpenJDK and OpenJFX
 * originally, this isn't needed; gradle files have to be adjusted if this goes
 * away
 * Ticket: https://github.com/javafxports/openjdk-jfx/issues/236
 */
public class Main {

    public static void main(String[] args) {
        Dkistcfg.main(args);
    }
}
