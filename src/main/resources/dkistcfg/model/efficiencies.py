import openpyxl
import matplotlib.pyplot as mp
import numpy as np

# ===> run
'''
%reset -sf
%load_ext autoreload
%autoreload 2
%run efficiencies
'''

def get_col(ws, cols):
  for row in ws.iter_rows():
    cells = [cell.value for (idx, cell) in enumerate(row) if (idx in cols and cell.value is not None)]
    yield cells

# open file
workbook = openpyxl.load_workbook('TN-0223 Flux Budget Data.xlsx')

# getting all sheet names
names = workbook.sheetnames
print(names)

# switch to appropriate sheet, and pull out wavelength
sheet = workbook[names[0]]
i_wavelength = np.asarray(list(get_col(sheet, [0]))[1:], dtype=np.float32)[:,0]
# switch to appropriate sheet, and pull out non-dichroic coatings
sheet = workbook[names[2]]
i_fresnel = np.asarray(list(get_col(sheet, [11]))[2:], dtype=np.float32)[:,0]
i_wbbar1 = np.asarray(list(get_col(sheet, [12]))[2:], dtype=np.float32)[:,0]
i_cm1 = np.asarray(list(get_col(sheet, [14]))[2:], dtype=np.float32)[:,0]
# switch to appropriate sheet, and pull out dichroic coatings
sheet = workbook[names[3]]
i_cbs465 = np.asarray(list(get_col(sheet, [1]))[2:], dtype=np.float32)[:,0]
i_cbs555 = np.asarray(list(get_col(sheet, [2]))[2:], dtype=np.float32)[:,0]
i_cbs643 = np.asarray(list(get_col(sheet, [3]))[2:], dtype=np.float32)[:,0]
i_cbs680 = np.asarray(list(get_col(sheet, [4]))[2:], dtype=np.float32)[:,0]
i_cbs950 = np.asarray(list(get_col(sheet, [5]))[2:], dtype=np.float32)[:,0]

# create interpolation wavelength
o_wavelength = np.arange(350,1900.05,0.05)
# create interpolated values on linear grid, and where necessary bring to % reflectance
o_fresnel = (1.-np.interp(o_wavelength, i_wavelength, i_fresnel))*100.
o_wbbar1 = (1.-np.interp(o_wavelength, i_wavelength, i_wbbar1))*100.
o_cm1 = np.interp(o_wavelength, i_wavelength, i_cm1)*100.
o_cbs465 = np.interp(o_wavelength, i_wavelength, i_cbs465)*100.
o_cbs555 = np.interp(o_wavelength, i_wavelength, i_cbs555)*100.
o_cbs643 = np.interp(o_wavelength, i_wavelength, i_cbs643)*100.
o_cbs680 = np.interp(o_wavelength, i_wavelength, i_cbs680)*100.
o_cbs950 = np.interp(o_wavelength, i_wavelength, i_cbs950)*100.

# save the arrays
np.savetxt("fresnel.txt",np.c_[o_wavelength,o_fresnel],fmt='%10.6f')
np.savetxt("wbbar1.txt",np.c_[o_wavelength,o_wbbar1],fmt='%10.6f')
np.savetxt("cm1.txt",np.c_[o_wavelength,o_cm1],fmt='%10.6f')
np.savetxt("cbs465.txt",np.c_[o_wavelength,o_cbs465],fmt='%10.6f')
np.savetxt("cbs555.txt",np.c_[o_wavelength,o_cbs555],fmt='%10.6f')
np.savetxt("cbs643.txt",np.c_[o_wavelength,o_cbs643],fmt='%10.6f')
np.savetxt("cbs680.txt",np.c_[o_wavelength,o_cbs680],fmt='%10.6f')
np.savetxt("cbs950.txt",np.c_[o_wavelength,o_cbs950],fmt='%10.6f')
