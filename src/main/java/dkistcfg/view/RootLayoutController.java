/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dkistcfg.view;

import dkistcfg.Analyze;
import dkistcfg.Dkistcfg;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;

/**
 * FXML Controller class
 *
 * @author fwoeger
 */
public class RootLayoutController implements Initializable {
  
  private Dkistcfg appCfg;

  /**
   * Initializes the controller class.
   * @param url
   * @param rb
   */
  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }  
  
  /**
   * Is called by the main application to give a reference back to itself.
   * 
   * @param appCfg
   */
  public void setAppCfg(Dkistcfg appCfg) {
    this.appCfg = appCfg;
  }

  /**
    * Creates an empty address book.
    */
    @FXML
    private void handleReset() {
      appCfg.getController().onReset();
    }

    /**
     * Opens a FileChooser to let the user select an address book to load.
     */
    @FXML
    private void handleOpen() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show open file dialog
        File file = fileChooser.showOpenDialog(appCfg.getPrimaryStage());

        if (file != null) {
            appCfg.loadCameraDataFromFile(file);
        }
    }

    /**
     * Saves the file to the person file that is currently open. If there is no
     * open file, the "save as" dialog is shown.
     */
    @FXML
    private void handleSave() {
        File cameraFile = appCfg.getCameraFilePath();
        if (cameraFile != null) {
            appCfg.saveCameraDataToFile(cameraFile);
        } else {
            handleSaveAs();
        }
    }

    /**
     * Opens a FileChooser to let the user select a file to save to.
     */
    @FXML
    private void handleSaveAs() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog
        File file = fileChooser.showSaveDialog(appCfg.getPrimaryStage());

        if (file != null) {
            // Make sure it has the correct extension
            if (!file.getPath().endsWith(".xml")) {
                file = new File(file.getPath() + ".xml");
            }
            appCfg.saveCameraDataToFile(file);
        }
    }

    /**
     * Opens a FileChooser to let the user select an address book to load.
     */
    @FXML
    private void handleLoadDirectoryData() {
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setTitle("Config File Directory To Analyze");

        // Show open file dialog
        File dir = dirChooser.showDialog(appCfg.getPrimaryStage());

        if (dir != null) {
            appCfg.analyze.loadCameraDataFromDir(dir);
        }

        // should be the last call in this function to clear out the use of the
        // BsConfig object in the CfgToolViewController class
        appCfg.cfgController.onReset();
    }

    /**
     * Opens an about dialog.
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("DKIST FIDO Configuration Tool");
        alert.setHeaderText("Contact Information");
        alert.setContentText("Author: Friedrich Woeger\nEmail: fwoeger@nso.edu\nWebsite: http://www.nso.edu");
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        // workaround for JavaFX bug
        alert.setResizable(true);
        alert.onShownProperty().addListener(e -> { 
          Platform.runLater(() -> alert.setResizable(false)); 
        });

        alert.showAndWait();
    }

    @FXML
    private void handleQuickHelp() {
        String quickHelpText =
            "* for each instrument, select the anticipated wavelengths (diagnostics) "
          + "for your use case (each input wavelength will be converted to an integer value)\n"
          + "NOTE: The ViSP wavelength input fields can accommodate ANY input "
          + "wavelength given manually. However:\n"
          + "NOTE: it is expected that the individual Instrument Performance Calculators "
          + "have either been used in advance, or will be used subsequently. The DKIST "
          + "FIDO Configuration Tool DOES NOT VERIFY use case feasibility within each instrument!\n\n"
          + "* IF there is a strong preference to observe a particular diagnostic "
          + "with a particular instrument, please adjust the Weight values accordingly "
          + "(each weight value will be converted to an integer value; the larger the "
          + "value, the more skewed the solution will be towards the favored "
          + "configuration while taking into account the remaining input values)\n\n"
          + "* IF already known from the Instrument Performance Calculators, "
          + "enter the Maximum Data Rate (in MiB) per requested camera, to compute whether "
          + "the use case is feasible for implementation in the DKIST Data Handling "
          + "System\n\n"
          + "* the output will update in real-time in the panel on the right "
          + "(in case there are mutliple solutions with the same success metric "
          + "a FIDO configuration can be selected with the drop down menu on "
          + "the right). The output indicates:\n"
          + "  - suggested FIDO configurations that maximize diagnostics that can be"
          + "observed with high efficiency\n"
          + "    [efficiencies and even diagnostics can change depending on selection!]\n"
          + "  - the efficiency with which each requested diagnostics per instrument can be observed\n"
          + "  - feasibility of implementation on the DKIST Data Handling System "
          + "(please observe the allowable Data Rate range)\n\n"
          + "* Expert mode is not implemented yet"
          + "";
        TextArea textArea = new TextArea(quickHelpText);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.initModality(Modality.NONE);
        alert.setResizable(true);
        alert.setTitle("Quick Help");
        alert.setHeaderText("Brief Usage Instructions");
        alert.getDialogPane().setPrefSize(800, 600);
        alert.getDialogPane().setContent(textArea);
        alert.show();
    }

    /**
     * Closes the application.
     */
    @FXML
    private void handleQuit() {
        System.exit(0);
    }  
}
