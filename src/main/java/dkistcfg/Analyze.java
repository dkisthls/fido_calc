/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dkistcfg;

import dkistcfg.model.BsConfig;
import dkistcfg.model.Camera;
import dkistcfg.model.CameraListWrapper;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.prefs.Preferences;
import javafx.application.Platform;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

/**
 *
 * @author fwoeger
 */
public class Analyze {
    // TODO:
    // function to load all files in dir, compute merit, and add configs to list (x)
    // function to compute wavelength distributions based on config
    // function to create histograms based on configs
    // function to create histograms based on wavelength distribution
  // keep all info from the config files in the directory
  File[] fileNames;
  private final ObservableList<ObservableList<Camera>> allCams = FXCollections.observableArrayList();
  private final ObservableList<ObservableList<ArrayList<Integer>>> allConfs = FXCollections.observableArrayList();
  // need a BsConfig object for some calculations
  BsConfig bsconf;

  // constructor - need to set the BsConfig object, otherwise too much resources
  // are used
  public Analyze(BsConfig bsC) {
      this.bsconf = bsC;
  }

  /**
   * Loads camera data from the specified file. The current camera data will
   * be replaced.
   * 
   * @param dir
   */
  public void loadCameraDataFromDir(File dir) {
      try {
          // clear filenames
          fileNames = null;
          allCams.clear();
          allConfs.clear();
          
          // only choose XML files in the directory
          FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
              String lowercaseName = name.toLowerCase();
                return lowercaseName.endsWith(".xml");
            }
          };
          fileNames = dir.listFiles(filter);
          
          JAXBContext context = JAXBContext
                  .newInstance(CameraListWrapper.class);
          Unmarshaller um = context.createUnmarshaller();
          for (File file : fileNames) {
            System.out.println(file);
            // clear camera data
            ObservableList<Camera> cameraData = FXCollections.observableArrayList();

            // Reading XML from the file and unmarshalling.
            CameraListWrapper wrapper = (CameraListWrapper) um.unmarshal(file);
            cameraData.addAll(wrapper.getCameras());
            // add to cam array
            allCams.add(cameraData);
            
            // compute configs for this cameras and add to solutions array
            bsconf.computeMerit(cameraData);
            allConfs.add(bsconf.getBestSolutions());
          }
          
          // show confirmation dialog
          Alert alert = new Alert(AlertType.CONFIRMATION);
          alert.setResizable(true);
          alert.setTitle("Success");
          alert.setHeaderText("Data have been loaded");
          alert.setContentText("The diretory\n" + 
              dir.getPath() + "\n" + "contained " + fileNames.length + " eligible files.\n" +
              "Please proceed with further anlysis.\n");
          alert.onShownProperty().addListener(ll -> { 
            Platform.runLater(() -> alert.setResizable(false)); 
          });
          alert.showAndWait();
          
          System.out.println(fileNames.length);
          System.out.println(allCams.size());
          System.out.println(allConfs.size());
      } catch (JAXBException e) { // catches ANY exception
          Alert alert = new Alert(AlertType.ERROR);
          alert.setTitle("Error");
          alert.setHeaderText("Could not load data");
          alert.setContentText("Could not load data from dir:\n" + dir.getPath());
          alert.setResizable(true);
          alert.onShownProperty().addListener(ll -> { 
            Platform.runLater(() -> alert.setResizable(false)); 
          });
          e.printStackTrace();

          alert.showAndWait();
      }
  }

}
